//
// Created by pieterjan on 01/08/2020.
//

#ifndef AISLINN_IO_IMAGE_H
#define AISLINN_IO_IMAGE_H

#include <string>
#include <vector>

#include "OpenImageIO/imageio.h"

namespace io
{
    /**
     * Small class representing an image loader based on OIIO.
     * The only goal of this class is to load images into the memory so we can then stream them to the GPU in Aislinn.
     * I/O is not really a big part of my goals for this project, so this received less attention than other parts.
     *
     * The explicit goal is for the instances of this class to be allowed to go out of scope and be deleted as soon as the data is on the GPU.
     * As such, there are no nice features, object manipulation, scene organization, ... It's just an I/O class.
     *
     * I did separate this from the main render code, so I could potentially swap it out one day.
     */
    template<typename datatype>
    class Image
    {
    public:
        Image(const std::string & filename, bool flipY = true, bool add_alpha = false) : m_imageSpecification(), m_data()
        {
            auto in = OIIO::ImageInput::open(filename);

            if(!in)
                throw std::invalid_argument("IO could not open the given filename: " + filename);

            m_imageSpecification = in->spec();
            uint32_t nchannels = m_imageSpecification.nchannels;
            nchannels = nchannels < 4 && add_alpha ? nchannels + 1 : nchannels;

            uint32_t imgPixelCount = m_imageSpecification.width * m_imageSpecification.height;
            uint32_t imgSize = imgPixelCount * nchannels;
            uint32_t scanlineElementCount = m_imageSpecification.width * nchannels;
            uint32_t scanlinesize = scanlineElementCount * sizeof(datatype);

            m_data.resize(imgSize);
            std::fill(m_data.begin(), m_data.end(), std::numeric_limits<datatype>::max());

            if(flipY)
            {
                // For some reason "read_image" doesn't work the way it is described in the documentation, so doing it manually:
                for(int y = 0; y < m_imageSpecification.height; ++y)
                {
                    in->read_scanline(y, 0, OIIO::BaseTypeFromC<datatype>::value, m_data.data() + ((m_imageSpecification.height - 1 - y) * scanlineElementCount),
                                     nchannels * sizeof(datatype));
                }

            }
            else in->read_image(OIIO::BaseTypeFromC<datatype>::value, m_data.data(),
                                nchannels*sizeof(datatype), scanlinesize, 0);

            in->close();
        }

        int width() const
        {
            return m_imageSpecification.width;
        }

        int height() const
        {
            return m_imageSpecification.height;
        }

        const std::vector<datatype>& data() const
        {
            return m_data;
        }
    private:
        OIIO::ImageSpec m_imageSpecification;
        std::vector<datatype> m_data;
    };
}

#endif //AISLINN_IO_IMAGE_H
