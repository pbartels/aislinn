//
// Created by pieterjan on 06/11/2020.
//

#ifndef AISLINN_IO_FILE_H
#define AISLINN_IO_FILE_H

#include <string>

namespace io
{
    std::string readFile(const std::string& filename);
}

#endif //AISLINN_IO_FILE_H
