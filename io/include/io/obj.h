//
// Created by pieterjan on 24/07/2020.
//

#ifndef AISLINN_IO_OBJ_H
#define AISLINN_IO_OBJ_H

#include <string>
#include <vector>

#include <gsl/span>

#include "tinyobjloader.h"

namespace io
{

    // data-backing. These allow us to pass spans to Aislinn. Aislinn never takes ownership of the CPU-side geometry
    //  I like it. Very clean: All the muddy geometry fiddling is kept away from the core renderer,
    //      who takes the geometry without ever copying it.
    struct Mesh
    {
        std::vector<float> positions, normals, uvs;
        std::vector<unsigned int> indices;
    };

    /**
     * Small class wrapping tiny object loader.
     * The only goal of this class is to load obj files into the memory so we can then stream them to the GPU in Aislinn.
     * I/O is not really a big part of my goals for this project, so this received less attention than other parts.
     *
     * The explicit goal is for the instances of this class to go out of scope and be deleted as soon as the geometry is on the GPU.
     * As such, there are no nice features, object manipulation, scene organization, ... It's just an I/O class.
     *
     * I did separate this from the main render code, so I could potentially swap it out one day.
     *
     * It worked well, Aislinn doesn't even depend on the geometry library. Let's keep it that way.
     */
    class Obj
    {
    public:
        // read in the geometry from a file. RAII
        Obj(const std::string & filename);

        // The only thing we need for Aislinn is a few functions that implicitly convert to Spans.
        //      At some point abstract this into a base class and use other I/O libraries without ever having to change Aislinn.
        const std::vector<Mesh>& meshes() const;


    private:
        // Aislinn uses indexed drawing (with EBOs) but OpenGL EBOs use 1 index for all attributes,
        //  so we need to fix that here. This function homogenizes the meshes.
        void createMeshes(const std::vector<tinyobj::shape_t>& shapes, const tinyobj::attrib_t & attrib);

        std::vector<Mesh> m_meshes;
    };

}

#endif //AISLINN_IO_OBJ_H
