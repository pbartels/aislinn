//
// Created by pieterjan on 24/07/2020.
//

#define TINYOBJLOADER_IMPLEMENTATION

#include <filesystem>
#include <iostream>
#include <map>

#include "io/obj.h"

namespace io
{
    Obj::Obj(const std::string &filename) : m_meshes()
    {
        // read in geometry
        if(!std::filesystem::exists(std::filesystem::path(filename)))
            throw std::invalid_argument("Filename given to obj loader class does not exist!");

        tinyobj::attrib_t attrib;

        // local variables we don't care about once the model is loaded.
        std::vector<tinyobj::shape_t> shapes;
        std::vector<tinyobj::material_t> materials;
        std::string warn;
        std::string err;

        // do the actual loading
        bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filename.c_str());

        // check for errors. For some reason this has to happen in three steps o.O
        if(!warn.empty())
        {
            std::cout << "Object loading gave a warning (or multiple): " << warn << std::endl;
        }

        if(!err.empty())
        {
            std::cerr << "Object loading resulted in one or more recoverable errors: " << err << std::endl;
        }

        if(!ret) throw std::invalid_argument("Obj loader class did not manage to read in obj file");

        std::cout << "Loading meshes with " << attrib.vertices.size() << " vertices from " << filename << std::endl;

        // This is important, as OpenGL uses only one index array.
        createMeshes(shapes, attrib);
    }

    const std::vector<Mesh>& Obj::meshes() const
    {
        return m_meshes;
    }

    void Obj::createMeshes(const std::vector<tinyobj::shape_t>& shapes, const tinyobj::attrib_t & attrib)
    {
        for(const tinyobj::shape_t& shape: shapes) {
            const std::vector<tinyobj::index_t>& indices = shape.mesh.indices;
            m_meshes.emplace_back();
            Mesh& currentMesh = m_meshes.back();

            std::cout << "\tCreating mesh with " << indices.size() << " vertices:" << std::endl;

            // structured binding (C++17): https://en.cppreference.com/w/cpp/language/structured_binding
            // the reference is important!!
            auto& [meshPositions, meshNormals, meshUvs, meshIndices] = currentMesh;

            // Really not that hard:
            // every existing combination gets a new index - took your sleepy self way too long to figure this out.
            std::map<std::tuple<int, int, int>, int> new_index_mapping;
            int nextIndex = 0;
            bool hasUv = (indices[0].texcoord_index > 0);

            for (tinyobj::index_t idx: indices) {
                int vertexIdx = idx.vertex_index;
                int normalIdx = idx.normal_index;
                int uvIdx = idx.texcoord_index;
                // here it doesn't matter if we have uvs or not.
                auto idxKey = std::make_tuple(vertexIdx, normalIdx, uvIdx);

                if (hasUv && uvIdx < 0) {
                    std::cout << "\t\tMesh has partial uv support. Turning off UVs for this mesh!" << std::endl;
                    hasUv = false;
                    meshUvs.clear();
                }

                if (new_index_mapping.count(idxKey) > 0) {
                    meshIndices.push_back(new_index_mapping[idxKey]); // keep the order of the pairs(!)
                } else {
                    // we place the position and normal next in the new position and normal lists
                    meshPositions.push_back(attrib.vertices[3 * vertexIdx]);
                    meshPositions.push_back(attrib.vertices[3 * vertexIdx + 1]);
                    meshPositions.push_back(attrib.vertices[3 * vertexIdx + 2]);
                    meshNormals.push_back(attrib.normals[3 * normalIdx]);
                    meshNormals.push_back(attrib.normals[3 * normalIdx + 1]);
                    meshNormals.push_back(attrib.normals[3 * normalIdx + 2]);
                    if (hasUv) {
                        // uvs are 2D
                        meshUvs.push_back(attrib.texcoords[2 * uvIdx]);
                        meshUvs.push_back(attrib.texcoords[2 * uvIdx + 1]);
                    }

                    // create new index array
                    meshIndices.push_back(nextIndex); // keep the order!
                    new_index_mapping[idxKey] = nextIndex; // ensure any other same pairs are assigned the new index
                    nextIndex++;
                }
            }

            std::cout << "\t\tCreated mesh with " << meshIndices.size() << " vertices (after homogenisation)."
                      << std::endl;
            if (hasUv) std::cout << "\t\tMesh has UVs included." << std::endl;
        }
    }

}