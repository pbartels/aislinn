//
// Created by pieterjan on 06/11/2020.
//

#include <filesystem>
#include <fstream>
#include <sstream>

#include "io/file.h"

std::string io::readFile(const std::string& filename)
{
    // check if the path exists before using it.
    //  Using std::filesystem from C++17
    if(!std::filesystem::exists(std::filesystem::path(filename)))
        throw std::invalid_argument("File given for shader source does not exist.");

    std::ifstream inputStream(filename, std::ios::binary);
    std::stringstream buffer;
    buffer << inputStream.rdbuf();
    return buffer.str();
}