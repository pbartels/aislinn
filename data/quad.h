//
// Created by pieterjan on 24/07/2020.
//

#ifndef AISLINN_DATA_QUAD_H
#define AISLINN_DATA_QUAD_H

#include <array>

namespace geometry::quad
{
    constexpr std::array<float, 12> positions = {
                                    -1.f, -1.f, 0.f,
                                     1.f, -1.f, 0.f,
                                    -1.f,  1.f, 0.f,
                                     1.f,  1.f, 0.f};

    constexpr std::array<float, 12> normals = {
                                    0.f, 0.f, -1.f,
                                    0.f, 0.f, -1.f,
                                    0.f, 0.f, -1.f,
                                    0.f, 0.f, -1.f,};

    constexpr std::array<float, 8> uvs = {
                                0.f, 0.f,
                                1.f, 0.f,
                                0.f, 1.f,
                                1.f, 1.f};

    constexpr std::array<unsigned int, 6> indices = {0, 1, 2, 2, 1, 3};
}

#endif //AISLINN_DATA_QUAD_H
