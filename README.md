# Aislinn renderer

Pet project of [Pieterjan Bartels](www.pieterjanbartels.be).

It is a sandbox project containing various real-time rendering projects to prepare myself for my job at Ubisoft. It also contains a notebook (notebook/ folder) with notes from real-time rendering study. 

So, the current goal right now is to:
* Read! A lot!
    * Computer Graphics books,
    * Computer Graphics papers,
    * C++ books, with a focus on modern C++ (C++17), 
    * Books/Documentation about Vulkan, CUDA and GPU programming.
* Implement what you read about in this repo. 

These two are equally important. Furthermore, avoid distractions and use Deep Work principles:
* Plan ahead,
* Avoid all distractions, focus on deep work, 
* Keep track of lead measures.

Current goals are kept on youtrack. 

Important to remember:

```It does not have to be perfect. Just do your best and keep learning.```

Current Dependencies (could become out of date, check cmakelists.txt): 
* Vulkan drivers
* glfw 
    * build from [source code](https://www.glfw.org/download.html)
* g++-9 (std::filesystem)
* gsl::span
    * Build and install from [Microsoft's implementation of the Guidelines Support Library.](https://github.com
    /microsoft/GSL)
* Eigen3
* It uses tinyobjloader in a separate library to load OBJ files. However, Aislinn itself does not depend on it
. Aislinn simply takes spans as input for its buffered meshes. It does not keep any CPU-side geometry (as an owner
). The dependency on tinyobjloader is separated into the io library.
* It uses OpenImageIO in a separate library to load images. However, Aislinn itself does not depend on it
. Aislinn simply takes spans as input for textures or other images. It does not keep any CPU-side images (as an owner
). As a result, the dependency on OpenImageIO is separated into the io library.


Rule of thumb C++ coding rules:
* Follow the [C++ core guidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
* RAII
* **DO NOT** use "using namespace" in header files
* Rule of three
* use const-correctness. DO IT.
* Use nullptr. Do not use 0 to assign to pointers. 
* Be mindful of memory usage and data ownership. Avoid copies where possible. Use RAII. Be critical.
* Do not pass in references to std::vector. Use gsl::span instead.
* Avoid hard-coding constants

OpenGL assumptions:
* Shaders used for drawing meshes have positions, normals, UVs (location 0, 1, 2 respectively), more to come.
* Never assume anything is still bound. Bind before usage. This makes unbinding optional.

## Current plans

 The current primary goal is to port Aislinn to Vulkan. The software is still very small, so this should still be
  easily doable. Something that is obvious immediately (and kind of the point anyway) is that I will need to add more
   layers than before, encapsulating all the backend instances (VkInstance, VkDevice, etc.) somehow. As always, the
    goal is to do this in modern C++ style, where everything is clean, easy-to-use, fast and type-safe. 
    
 Early idea:I will have to wrap the instance and device into some sort of class. I don't want this class to be global
 . Instead, we can let the user this class (i.e. a "Renderer" class, or something similar). There are two issues to
  consider when designing this:
  * How do I want to use Aislinn eventually? What inputs should Aislinn have? How much flexibility should the developer
   have?
  * Make sure the Aislinn internals are cleanly coded, according to the pragmatic programmer book!

Make it easy to maintain, make it easy to use. 

"Good design is Easier to Change than Bad Design."

Easy to Change. ETC.