//
// Created by pieterjan on 02/12/2020.
//

#include <iostream>
#include <glfwbackend.h>

#include "glfwbackend.h"

using namespace glfwbackend;


GlfwBackEnd::GlfwBackEnd(const char* title, int windowWidth, int windowHeight) : m_window(nullptr)
{
    glfwInit();
    glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API ); // no api implies it is not creating an opengl context
    glfwWindowHint( GLFW_RESIZABLE, GLFW_FALSE );
    m_window = glfwCreateWindow( windowWidth, windowHeight, title, nullptr, nullptr);
    if(m_window == nullptr)
    {
        glfwTerminate();
        throw std::runtime_error("Failed to create GLFW window");
    }
}

GlfwBackEnd::~GlfwBackEnd()
{
    glfwTerminate();
}

void GlfwBackEnd::createSurface(VkInstance instance, VkSurfaceKHR* surface)
{
    VkResult result = glfwCreateWindowSurface(instance, m_window, nullptr, surface);
    if(result != VK_SUCCESS) throw std::runtime_error("GLFW surface creation failed.");
}

void GlfwBackEnd::logBackEndDetails() const
{
    std::cout << "GLFW BackEnd: " << glfwVulkanSupported() << std::endl;
    std::cout << "\tVulkan supported: " << glfwVulkanSupported() << std::endl;

    uint32_t count;
    const char ** extensions = glfwGetRequiredInstanceExtensions(&count);
    std::cout << "\trequired extensions: " << count << std::endl;
    for(uint32_t i =0; i < count; ++i)
    {
        std::cout << "\t\t" << i << ": " << extensions[i] << std::endl;
    }
}

bool GlfwBackEnd::keepRendering()
{
    glfwPollEvents();
    return !glfwWindowShouldClose(m_window);
}