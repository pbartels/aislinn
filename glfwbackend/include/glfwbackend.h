//
// Created by pieterjan on 02/12/2020.
//

#ifndef AISLINN_GLFWBACKEND_H
#define AISLINN_GLFWBACKEND_H

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "ivulkansurfacebackend.h"

namespace glfwbackend
{

class GlfwBackEnd : public aislinn::IVulkanSurfaceBackEnd
{
public:
    GlfwBackEnd(const char* title, int windowWidth, int windowHeight);
    ~GlfwBackEnd();

    virtual void createSurface(VkInstance instance, VkSurfaceKHR* surface);
    bool keepRendering();

    void logBackEndDetails() const;
private:
    GLFWwindow* m_window;
};

}

#endif //AISLINN_GLFWBACKEND_H
