#version 450 core
#extension GL_KHR_vulkan_glsl : enable

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUvs;

layout(location = 0) out vec3 wsPosition;
layout(location = 1) out vec2 uvs;
layout(location = 2) out float instance;

void main() {
    vec3 basePosition = vec3(-1, -1, 0.5);
    gl_Position = vec4(basePosition.xy + (0.25 * position.xy) + (0.25 * gl_InstanceIndex.xx), 0.5, 1.0);
    wsPosition = gl_Position.xyz / gl_Position.w;
    uvs = vertexUvs;
    instance = float(gl_InstanceIndex);
}
