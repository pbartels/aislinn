//
// Created by pieterjan on 04/01/2021.
//

#include "aislinn/rendertask.h"
#include "aislinn/geometry/vertexconfig.h"

#include "io/file.h"

using namespace aislinn;

RenderTask::RenderTask(VkDevice vulkanDevice,
                       aislinn::val::DeviceMemoryBank& memoryBank,
                       aislinn::GeometryManager& geometryManager,
                       VkFormat format,
                       uint32_t windowWidth,
                       uint32_t windowHeight,
                       Span<aislinn::val::MovableVkImageView> targetImageViews) :
	m_device(vulkanDevice),
	m_memoryBank(std::addressof(memoryBank)),
	m_geometryManager(std::addressof(geometryManager))
{
    /** Create the shader modules and shader stages **/

    // start with the vertex shader
    std::string vertShaderCode = io::readFile("aislinnShaders/simple.vert.spv");
    m_vertShaderModule = {m_device, {VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, nullptr, 0,
                                         vertShaderCode.size(), (uint32_t*) vertShaderCode.c_str()}};
    // then do the fragment shader
    std::string fragShaderCode = io::readFile("aislinnShaders/simple.frag.spv");
    m_fragShaderModule = {vulkanDevice, {VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, nullptr, 0,
                                         fragShaderCode.size(), (uint32_t*) fragShaderCode.c_str()}};

    // we then wrap these shaders into a shader stage creation info object.
    VkPipelineShaderStageCreateInfo shaderStages[2] = {{VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                                                               nullptr, 0,
                                                               VK_SHADER_STAGE_VERTEX_BIT,
                                                               m_vertShaderModule.handle(), "main", nullptr},
                                                       {VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                                                               nullptr, 0,
                                                               VK_SHADER_STAGE_FRAGMENT_BIT,
                                                               m_fragShaderModule.handle(), "main", nullptr}};

    /** setting up a render pass **/
    VkAttachmentDescription outputAttachmentDescription = {0,
                                                           format,
                                                           VK_SAMPLE_COUNT_1_BIT, // Sample count
                                                           VK_ATTACHMENT_LOAD_OP_CLEAR, // load op: clear the framebuffer at the start
                                                           VK_ATTACHMENT_STORE_OP_STORE,
                                                           VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                                                           VK_ATTACHMENT_STORE_OP_DONT_CARE,
                                                           VK_IMAGE_LAYOUT_UNDEFINED,
                                                           VK_IMAGE_LAYOUT_PRESENT_SRC_KHR};

    // First part in setting this up is describing the images involved.
    // create a reference to it for use in the subpass (see below)
    VkAttachmentReference outputAttachmentReference = {0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};

    // Next up we describe the subpasses.
    // Again, for now we have only one subpass that renders to our color attachment described above.
    VkSubpassDescription subpassDescription = {0,
                                               VK_PIPELINE_BIND_POINT_GRAPHICS, // as opposed to compute
                                               0, nullptr, // input attachments
                                               1, &outputAttachmentReference, // output attachments
                                               nullptr, nullptr, // no need for these for now.
                                               0,
                                               nullptr }; // also not needed for now.

    m_renderpass = {m_device, {VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
                                   nullptr,0, 1,
                                   &outputAttachmentDescription,
                                   1, &subpassDescription,
                                   0, nullptr}};


    /** 12. Create a graphics pipeline object **/
    // step 1+2: We need to feed in vertex input, which we describe through an aislinn VertexInputConfiguration
    VkPipelineVertexInputStateCreateInfo vertexInputState = s_vertexConfiguration.vertexInputStateCreateInfo();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo = s_vertexConfiguration.inputAssemblyStateCreateInfo();


    // step 4: create a viewport state
    VkViewport viewport = {0, 0, (float) windowWidth, (float) windowHeight, 0.f, 1.f};
    VkRect2D scissor = {{0, 0}, {windowWidth, windowHeight}};


    VkPipelineViewportStateCreateInfo viewportStateCreateInfo = {VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
                                                                 nullptr, 0,
                                                                 1, &viewport,
                                                                 1, &scissor};

    // Define the rasterization state
    VkPipelineRasterizationStateCreateInfo rasterizationStateCreateInfo = {VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
                                                                           nullptr, 0,
                                                                           VK_FALSE, VK_FALSE,
                                                                           VK_POLYGON_MODE_FILL,
                                                                           VK_CULL_MODE_NONE,
                                                                           VK_FRONT_FACE_COUNTER_CLOCKWISE,
                                                                           VK_FALSE, 0, 0, 0,
                                                                           1.f};
    // create a color blend state
    VkPipelineColorBlendAttachmentState blendAttachmentState = {VK_FALSE,
                                                                VK_BLEND_FACTOR_SRC_COLOR,
                                                                VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR,
                                                                VK_BLEND_OP_ADD,
                                                                VK_BLEND_FACTOR_ONE,
                                                                VK_BLEND_FACTOR_ZERO,
                                                                VK_BLEND_OP_ADD,
                                                                VK_COLOR_COMPONENT_A_BIT
                                                                | VK_COLOR_COMPONENT_R_BIT
                                                                | VK_COLOR_COMPONENT_G_BIT
                                                                | VK_COLOR_COMPONENT_B_BIT};

    VkPipelineMultisampleStateCreateInfo multisampleStateCreateInfo = {VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
                                                                       nullptr, 0,
                                                                       VK_SAMPLE_COUNT_1_BIT,
                                                                       VK_FALSE,1,
                                                                       nullptr,
                                                                       VK_FALSE, VK_FALSE};

    VkPipelineColorBlendStateCreateInfo colorBlendStateCreateInfo = {VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
                                                                     nullptr, 0,
                                                                     VK_FALSE,
                                                                     VK_LOGIC_OP_COPY,
                                                                     1, &blendAttachmentState,
                                                                     {1, 1, 1, 1}};
    // Create a push constant range for a uniform (e.g. time)
    VkPushConstantRange timeUniform = {VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(float)};

    m_pipelineLayout  = {m_device,
                         {VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
                          nullptr, 0,
                          0, nullptr,
                          1, &timeUniform}};

    m_pipeline = {m_device, {VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
                                           nullptr, 0,
                                           2, &shaderStages[0],
                                           &vertexInputState,
                                           &inputAssemblyStateCreateInfo,
                                           nullptr,
                                           &viewportStateCreateInfo,
                                           &rasterizationStateCreateInfo,
                                           &multisampleStateCreateInfo,
                                           nullptr,
                                           &colorBlendStateCreateInfo,
                                           nullptr,
                                           m_pipelineLayout.handle(),
                                           m_renderpass.handle(),
                                           0,
                                           VK_NULL_HANDLE,
                                           0}};


    /** create a framebuffer **/
    for(auto& imageView: targetImageViews)
    {
        m_framebuffers.push_back({m_device, {VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
											 nullptr, 0,
                                             m_renderpass.handle(),
                                             1, imageView.pointer(),
                                             windowWidth, windowHeight, 1}});
    }

    // Command buffer/render pass information that is the same every loop.
    VkClearColorValue clearColorValue = {0, 0, 0, 1};
    m_clearValue = {clearColorValue};
    VkOffset2D o2D = {0, 0 };
    VkExtent2D e2D = {windowWidth, windowHeight};
    m_r2D = {o2D, e2D};

}

bool RenderTask::valid() const
{
	return (m_device != VK_NULL_HANDLE && m_geometryManager != nullptr && m_memoryBank != nullptr);
}

void RenderTask::buildCommandBuffer(VkCommandBuffer commandBuffer,
                                    size_t nextImageViewIndex,
									Span<const RenderInstance> instancesToRender,
                                    float time) const
{

    assert(valid());

    VkCommandBufferBeginInfo commandBufferBeginInfo = {VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                                                       nullptr,
                                                       0, // this means we can reuse it
                                                       nullptr};

    VkRenderPassBeginInfo renderpassBeginInfo = {VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO, nullptr,
                                                 m_renderpass.handle(), m_framebuffers[nextImageViewIndex].handle(),
                                                 m_r2D, 1, &m_clearValue};

    // Create the command buffer
    // this implicitly resets the command buffer! See spec.
    vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
    vkCmdBeginRenderPass(commandBuffer, &renderpassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline.handle());

    vkCmdPushConstants(commandBuffer, m_pipelineLayout.handle(), VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(float), &time);

    for(const RenderInstance& instance: instancesToRender)
    {
		// TODO:  Set transforms (for some render tasks this will not be useful)

        m_geometryManager->ensureLoaded(m_device, *m_memoryBank, instance.meshID());

        m_geometryManager->meshBufferSet(instance.meshID()).buildCommandBuffer(commandBuffer, instance.instanceCount());
    }

    vkCmdEndRenderPass(commandBuffer);
    vkEndCommandBuffer(commandBuffer);
}