//
// Created by pieterjan on 24/11/2021.
//

#include <iostream>

#include "aislinn/renderer.h"

using namespace aislinn;


Renderer::Renderer(SurfaceInitializer surfaceInitializer) :
	m_vkBackend(aislinn::val::configfactory::validationConfig()),
	m_memoryBank(m_vkBackend.vulkanPhysicalDevice(), m_vkBackend.vulkanDevice()),
	m_queue(m_vkBackend.queue(0)),
	m_commandPool(m_vkBackend.vulkanDevice(), aislinn::val::vulkanstructs::commandPoolCreateInfo(m_queue.family())),
	m_commandBuffer(m_vkBackend.vulkanDevice(), aislinn::val::vulkanstructs::commandBufferAllocateInfo(m_commandPool.handle(), /*primary*/true)),
	m_surface(m_vkBackend.vulkanInstance(), surfaceInitializer),
	m_swapchain(),
	m_swapchainImageViews(),
	m_geometryManager(),
	m_renderTask()
{
	m_vkBackend.logDetails();
    logSurfaceDetails();


    // We need to check if the physical device supports the surface we've created before creating the swapchain
    // It's not a hard constraint but the validation layer complains if we don't
    if(!validateSurface())
		throw std::runtime_error("The created surface is not supported by the Vulkan Instance created by Aislinn.");

    initializeSwapchain();
}

RenderMesh Renderer::createRenderMesh(MeshData meshData)
{
	return m_geometryManager.CreateRenderMesh(meshData);
}


// Render functionality
void Renderer::render(Span<const RenderInstance> instances)
{
	static float time = 0;
	// we create a semaphore to wait on the image to be ready.
	aislinn::val::ImmovableVkSemaphore imageReadySemaphore(m_vkBackend.vulkanDevice(), { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr, 0 });


	// get the index of the next buffer to use from the swapchain.
	uint32_t nextImageIndex;
	vkAcquireNextImageKHR(m_vkBackend.vulkanDevice(), m_swapchain.handle(), UINT64_MAX, imageReadySemaphore.handle(), VK_NULL_HANDLE, &nextImageIndex);

	m_renderTask.buildCommandBuffer(m_commandBuffer.handle(), nextImageIndex, instances, time);

	// wait until the framebuffer is available before writing into it.
	VkPipelineStageFlags bufferWrite = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	VkSubmitInfo submitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr,
		                        1, imageReadySemaphore.pointer(), &bufferWrite,
			                    1, m_commandBuffer.pointer(), 0, nullptr };

	aislinn::val::ImmovableVkFence renderFence(m_vkBackend.vulkanDevice(), { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr, 0 });

	vkQueueSubmit(m_queue.handle(), 1, &submitInfo, renderFence.handle());

	// Wait for rendering to be done.
	vkWaitForFences(m_vkBackend.vulkanDevice(), 1, renderFence.pointer(), VK_TRUE, UINT64_MAX);

	// then present
	VkPresentInfoKHR presentInfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, nullptr,
		                             0, nullptr,
			                         1, m_swapchain.pointer(),
				                     &nextImageIndex, nullptr };
	vkQueuePresentKHR(m_queue.handle(), &presentInfo);
	time += 0.05f;
}

void Renderer::logSurfaceDetails() const
{
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_vkBackend.vulkanPhysicalDevice(), m_surface.handle(), &surfaceCapabilities);
    std::cout << "The selected physical device has the following capabilities for the created surface:" << std::endl;
    std::cout << "\t Image count (min, max): " << surfaceCapabilities.minImageCount << ", "
        << surfaceCapabilities.maxImageCount << std::endl;
    std::cout << "\t Supported usage flags: " << surfaceCapabilities.supportedUsageFlags << std::endl;

    // Check the available imageFormats for the surface
    uint32_t surfaceFormatCount;
    // get the count first
    vkGetPhysicalDeviceSurfaceFormatsKHR(m_vkBackend.vulkanPhysicalDevice(), m_surface.handle(), &surfaceFormatCount, nullptr);
    std::vector<VkSurfaceFormatKHR> surfaceFormats(surfaceFormatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(m_vkBackend.vulkanPhysicalDevice(), m_surface.handle(), &surfaceFormatCount,
        surfaceFormats.data());
    std::cout << "\t Available surface formats: " << surfaceFormatCount << std::endl;
    for (VkSurfaceFormatKHR format : surfaceFormats)
    {
        std::cout << "\t\t" << "Format: " << format.format << ", Colorspace: " << format.colorSpace << std::endl;
    }
}

bool Renderer::validateSurface() const
{
    VkBool32 supported;
    vkGetPhysicalDeviceSurfaceSupportKHR(m_vkBackend.vulkanPhysicalDevice(), 0, m_surface.handle(), &supported);
    return (supported == VK_TRUE);
}

void Renderer::initializeSwapchain()
{
	// We need to know the formats supported by the surface, so we get them from Vulkan:
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_vkBackend.vulkanPhysicalDevice(), m_surface.handle(), &surfaceCapabilities);
    // Get the available imageFormats for the surface
    uint32_t surfaceFormatCount;
    // get the count first
    vkGetPhysicalDeviceSurfaceFormatsKHR(m_vkBackend.vulkanPhysicalDevice(), m_surface.handle(), &surfaceFormatCount, nullptr);
    std::vector<VkSurfaceFormatKHR> surfaceFormats(surfaceFormatCount);
	// then the actual information
    vkGetPhysicalDeviceSurfaceFormatsKHR(m_vkBackend.vulkanPhysicalDevice(), m_surface.handle(), &surfaceFormatCount, surfaceFormats.data());

	m_swapchain = { m_vkBackend.vulkanDevice(),
					{   VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
						nullptr,
						0,
						m_surface.handle(),
			            3,
						surfaceFormats[1].format,
						surfaceFormats[1].colorSpace,
						{800, 800},
						1,
						VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
						VK_SHARING_MODE_EXCLUSIVE,
						// the next two fields are ignored with that sharing mode
						0,
						nullptr,
						VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
						VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
						VK_PRESENT_MODE_FIFO_KHR,
						VK_TRUE,
						VK_NULL_HANDLE
					}
				  };

    // get the swapchain images
    uint32_t swapchainImageCount;
    vkGetSwapchainImagesKHR(m_vkBackend.vulkanDevice(), m_swapchain.handle(), &swapchainImageCount, nullptr);
    std::vector<VkImage> swapchainImages(swapchainImageCount);
    vkGetSwapchainImagesKHR(m_vkBackend.vulkanDevice(), m_swapchain.handle(), &swapchainImageCount, swapchainImages.data());

    for (uint32_t i = 0; i < swapchainImageCount; ++i)
    {
        m_swapchainImageViews[i] = { m_vkBackend.vulkanDevice(),
                                     {
                                     	VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
										nullptr, 0,
										swapchainImages[i], VK_IMAGE_VIEW_TYPE_2D,
										surfaceFormats[1].format,
										{
                                     		VK_COMPONENT_SWIZZLE_R,
											VK_COMPONENT_SWIZZLE_G,
											VK_COMPONENT_SWIZZLE_B,
											VK_COMPONENT_SWIZZLE_A
										},
										{
                                     		VK_IMAGE_ASPECT_COLOR_BIT,
											0, 1,
											0, 1
										}
                                     }
								   };
    }

	// With the swapchain created, we can update the RenderTask
    m_renderTask = RenderTask(m_vkBackend.vulkanDevice(), m_memoryBank, m_geometryManager, surfaceFormats[1].format, 800, 800, m_swapchainImageViews);
}