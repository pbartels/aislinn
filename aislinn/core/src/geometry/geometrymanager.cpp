//
// Created by pieterjan on 16/11/2021.
//

#include "assert.h"

#include "aislinn/geometry/geometrymanager.h"
#include "aislinn/geometry/vertexconfig.h"

using namespace aislinn;

RenderMesh::RenderMesh(GeometryManager* manager, MeshID uid) :
	m_geometryManager(manager),
	m_uid(uid)
{}

RenderMesh::RenderMesh(RenderMesh&& other) noexcept :
	m_geometryManager(other.m_geometryManager),
	m_uid(other.m_uid)
{
	other.m_geometryManager = nullptr;
	other.m_uid = -1;
}

RenderMesh& RenderMesh::operator=(RenderMesh&& other) noexcept
{
	if (this != std::addressof(other))
	{
		// release our resource if needed 
		invalidate();

		// steal from other 
		m_geometryManager = other.m_geometryManager;
		m_uid = other.m_uid;

		// Make sure other no longer points to something useful 
		other.m_geometryManager = nullptr;
		other.m_uid = -1;
	}

	return *this;
}

RenderMesh::~RenderMesh()
{
	invalidate();
}


void RenderMesh::updateData(MeshData data)
{
	if(m_geometryManager)
	{
		// Signal geometry manager that this mesh needs to be updated.
		// Make a copy straight away.
		// Streaming it into GPU can happen later.
		m_geometryManager->updateRenderMesh(m_uid, data);
	}
}


void RenderMesh::invalidate() noexcept
{
	if (m_geometryManager)
	{
		// Signal geometry manager that this mesh is no longer used
		m_geometryManager->cleanUp(m_uid);
	}
	m_geometryManager = nullptr;
	m_uid = -1;
}


GeometryManager::GeometryManager() : m_meshRecords(0)
{}

GeometryManager::GeometryManager(GeometryManager&& other) noexcept : m_meshRecords(0)
{
	*this = std::move(other);
}

GeometryManager& GeometryManager::operator=(GeometryManager&& other) noexcept
{
	if(this != std::addressof(other))
	{
		// We allow moving in this class, but *only* if there are no MeshRecords, since they would leave dangling references.
		bool liveReference = false;
		for (const auto& record : other.m_meshRecords)
			liveReference = liveReference || record.isUsed;
		for (const auto& record : m_meshRecords)
			liveReference = liveReference || record.isUsed;
		assert(!liveReference);
	}

	return *this;
}

RenderMesh GeometryManager::CreateRenderMesh(MeshData data)
{
	m_meshRecords.push_back(MeshRecord{true, false, false, 0, 0, {}, {}, {}, {}, {}, {}, {}});
	RenderMesh resultingMesh(this, m_meshRecords.size() - 1);
	updateRenderMesh(resultingMesh.uid(), data);
	return resultingMesh;
}

void GeometryManager::updateRenderMesh(MeshID meshID, MeshData data)
{
	MeshRecord& record = m_meshRecords[(size_t) meshID];
	record.vertexCount = (int) data.m_positions.size() / 3;
	record.indexCount = (int) data.m_indices.size();
	record.positionData.resize(data.m_positions.size());
	record.uvData.resize(data.m_uvs.size());
	record.indexData.resize(data.m_indices.size());
	std::copy(data.m_positions.begin(), data.m_positions.end(), record.positionData.begin());
	std::copy(data.m_uvs.begin(), data.m_uvs.end(), record.uvData.begin());
	std::copy(data.m_indices.begin(), data.m_indices.end(), record.indexData.begin());
	// Make sure we flag this is as dirty. 
	record.isConsistent = false;
}

void GeometryManager::ensureLoaded(VkDevice device, aislinn::val::DeviceMemoryBank& memoryBank, MeshID meshId)
{
	MeshRecord& record = m_meshRecords[(size_t)meshId];
	// if this function is called on a mesh that is not in use, something went wrong. 
	assert(record.isUsed);
	
	if(!record.isOnGPU || !record.isConsistent)
	{

		record.positionBuffer = aislinn::Buffer(device, memoryBank, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, record.positionData);
		record.uvBuffer = aislinn::Buffer(device, memoryBank, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, record.uvData);
		record.indexBuffer = aislinn::Buffer(device, memoryBank, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, record.indexData);
		record.bufferSet = aislinn::VertexInputBufferSet((uint32_t)record.indexCount, record.indexBuffer, {record.positionBuffer, record.uvBuffer});
		assert(s_vertexConfiguration.isValidBufferSet(record.bufferSet));

		record.isOnGPU = true;
		record.isConsistent = true;
	}
}

aislinn::VertexInputBufferSet GeometryManager::meshBufferSet(MeshID meshID)
{
	MeshRecord& record = m_meshRecords[(size_t)meshID];
	return record.bufferSet;
}

void GeometryManager::cleanUp(MeshID meshId) noexcept
{
	MeshRecord& record = m_meshRecords[(size_t)meshId];
	record.positionData.clear();
	record.uvData.clear();
	record.indexData.clear();
	record.vertexCount = 0;
	record.indexCount = 0;
	// if this function is called on a mesh that is not in use, something went wrong. 
	assert(record.isUsed);
	if(record.isOnGPU)
	{
		// assign empty buffers, let RAII take care of clean up.
		record.positionBuffer = aislinn::Buffer();
		record.uvBuffer = aislinn::Buffer();
		record.indexBuffer = aislinn::Buffer();
	}
	record.bufferSet = aislinn::VertexInputBufferSet();
	record.isUsed = false;
	record.isConsistent = true;
	record.isOnGPU = false;
}