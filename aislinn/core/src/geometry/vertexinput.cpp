//
// Created by pieterjan on 22/12/2020.
//

#include "aislinn/geometry/vertexinput.h"

using namespace aislinn;

VertexInputBufferSet::VertexInputBufferSet(uint32_t elementCount,
                                           std::initializer_list<std::reference_wrapper<const Buffer>> buffers) :
                                           VertexInputBufferSet(elementCount, nullptr, {buffers.begin(), buffers.size()})

{}

VertexInputBufferSet::VertexInputBufferSet(uint32_t elementCount,
                                           const Buffer& indexBuffer,
                                           std::initializer_list<std::reference_wrapper<const Buffer>> buffers) :
                                           VertexInputBufferSet(elementCount, &indexBuffer, {buffers.begin(), buffers.size()})
{}

void VertexInputBufferSet::buildCommandBuffer(VkCommandBuffer commandBuffer, uint32_t instanceCount) const
{

    vkCmdBindVertexBuffers(commandBuffer, 0, (uint32_t) m_dataBuffers.size(), m_dataBuffers.data(), m_offsets.data());
    if(m_indexBuffer == nullptr)
    {
        vkCmdDraw(commandBuffer, m_elementCount, instanceCount, 0, 0);
    }
    else
    {
        vkCmdBindIndexBuffer(commandBuffer, m_indexBuffer->handle(), 0, VK_INDEX_TYPE_UINT32);
        vkCmdDrawIndexed(commandBuffer, m_elementCount, instanceCount, 0, 0, 0);
    }
}

VertexInputBufferSet::VertexInputBufferSet(uint32_t elementCount,
                                           const Buffer* indexBuffer,
                                           Span<const std::reference_wrapper<const Buffer>> buffers) :
                                           m_elementCount(elementCount),
                                           m_indexBuffer(indexBuffer),
                                           m_dataBuffers(),
                                           m_offsets(buffers.size(), 0)
{
    m_dataBuffers.resize(buffers.size());
    for(size_t i = 0; i < buffers.size(); ++i) m_dataBuffers[i] = buffers[i].get().handle();
}



VertexInputConfiguration::VertexInputBufferDescriptor::VertexInputBufferDescriptor(uint32_t elementSizeInBytes,
                                                                                   std::initializer_list<ShaderAttributeDescriptor> descriptors) :
                                                                     m_elementSizeInBytes(elementSizeInBytes),
                                                                     m_shaderAttributes(descriptors)

{}

VertexInputConfiguration::VertexInputBufferDescriptor::VertexInputBufferDescriptor(uint32_t elementSizeInBytes,
                                                                                   ShaderAttributeDescriptor descriptor) :
                                                                     m_elementSizeInBytes(elementSizeInBytes),
                                                                     m_shaderAttributes(1, descriptor)
{}

VertexInputConfiguration::VertexInputConfiguration(std::initializer_list<VertexInputBufferDescriptor> descriptors,
                                                   VkPrimitiveTopology topology, bool primitiveRestart) :
                                            m_vertexInputBindingDescriptions(),
                                            m_vertexInputAttributeDescriptions(),
                                            m_topology(topology),
                                            m_primitiveRestart(primitiveRestart)
{
    addDescriptors(descriptors);
}

VertexInputConfiguration::VertexInputConfiguration(Span<VertexInputBufferDescriptor> descriptors,
                                                   VkPrimitiveTopology topology, bool primitiveRestart) :
                                            m_vertexInputBindingDescriptions(),
                                            m_vertexInputAttributeDescriptions(),
                                            m_topology(topology),
                                            m_primitiveRestart(primitiveRestart)
{
    addDescriptors(descriptors);
}

void VertexInputConfiguration::addDescriptor(VertexInputBufferDescriptor descriptor)
{
    addDescriptors(Span<const VertexInputBufferDescriptor>{&descriptor, 1});
}

void VertexInputConfiguration::addDescriptor(uint32_t elementSizeInBytes, std::initializer_list<VertexInputConfiguration::ShaderAttributeDescriptor> shaderAttributes)
{
    addDescriptor(VertexInputConfiguration::VertexInputBufferDescriptor{elementSizeInBytes, shaderAttributes});
}

void VertexInputConfiguration::addDescriptors(std::initializer_list<VertexInputBufferDescriptor> descriptors)
{
    addDescriptors(Span<const VertexInputBufferDescriptor>{descriptors.begin(), descriptors.size()});
}

void VertexInputConfiguration::addDescriptors(Span<const VertexInputBufferDescriptor> descriptors)
{
    for(VertexInputBufferDescriptor descriptor: descriptors)
    {
        uint32_t bufferBinding = (uint32_t) m_vertexInputBindingDescriptions.size();
        m_vertexInputBindingDescriptions.push_back({bufferBinding, descriptor.m_elementSizeInBytes, VK_VERTEX_INPUT_RATE_VERTEX});
        for(const ShaderAttributeDescriptor& attribute: descriptor.m_shaderAttributes)
        {
            m_vertexInputAttributeDescriptions.push_back({attribute.m_location, bufferBinding, attribute.m_format, attribute.m_offset});
        }
    }
}

bool VertexInputConfiguration::isValidBufferSet(const VertexInputBufferSet& bufferSet) const
{
    // For now, all we check is whether the buffer set has the right number of buffers
    return m_vertexInputBindingDescriptions.size() == bufferSet.m_dataBuffers.size();
}

Span<const VkVertexInputBindingDescription> VertexInputConfiguration::vertexInputBindingDescriptions() const
{
    return m_vertexInputBindingDescriptions;
}

Span<const VkVertexInputAttributeDescription> VertexInputConfiguration::vertexInputAttributeDescription() const
{
    return m_vertexInputAttributeDescriptions;
}

VkPipelineVertexInputStateCreateInfo VertexInputConfiguration::vertexInputStateCreateInfo() const
{
    return {VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            nullptr, 0,
            (uint32_t) m_vertexInputBindingDescriptions.size(),
            m_vertexInputBindingDescriptions.data(),
            (uint32_t) m_vertexInputAttributeDescriptions.size(),
            m_vertexInputAttributeDescriptions.data()};
}

VkPipelineInputAssemblyStateCreateInfo VertexInputConfiguration::inputAssemblyStateCreateInfo() const
{
    return {VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            nullptr, 0, m_topology,
            (VkBool32) (m_primitiveRestart ? VK_TRUE : VK_FALSE)};
}