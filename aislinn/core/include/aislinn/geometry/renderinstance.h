//
// Created by pieterjan on 16/11/2021.
//

#ifndef AISLINN_GEOMETRY_RENDERINSTANCE_H
#define AISLINN_GEOMETRY_RENDERINSTANCE_H

#include "aislinnutilities.h"
#include "geometrymanager.h"

namespace aislinn
{

// Lightweight objects to build a scene representation
// They can be copied freely. It is up to the user to make sure no mesh can go out of scope if it is still referenced be a RenderInstance that is alive.
class RenderInstance
{

public:
	inline RenderInstance(const RenderMesh& rendermesh, uint32_t instanceCount = 1) : m_meshID(rendermesh.uid()), m_instanceCount(instanceCount) {}

	inline MeshID meshID() const {return m_meshID;}
	inline uint32_t instanceCount() const{return m_instanceCount;}

private:
	// Should have a transform (How do we represent transforms?)
	MeshID m_meshID;
	uint32_t m_instanceCount; // amount of times the mesh should be drawn for this specific instance
};

}


#endif 