//
// Created by pieterjan on 22/12/2020.
//

#include "vertexinput.h"

namespace aislinn
{
	// Right now, Aislinn enforces a specific vertex config:
	// We read three floats from one buffer for positions,
	// We read two floats from another buffer for UVs
	inline const static VertexInputConfiguration s_vertexConfiguration = { {/*Element size in bytes*/ 3 * sizeof(float),/*Shader attribute descriptors*/{0, VK_FORMAT_R32G32B32_SFLOAT, 0}},
																		   {/*Element size in bytes*/ 2 * sizeof(float),/*Shader attribute descriptors*/{1, VK_FORMAT_R32G32_SFLOAT, 0}} };
}