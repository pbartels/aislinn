//
// Created by pieterjan on 22/12/2020.
//

#ifndef AISLINN_VERTEXINPUT_H
#define AISLINN_VERTEXINPUT_H

#include <vector>

#include "vulkan/vulkan.h"

#include "aislinnutilities.h"

#include "aislinn/typedefs.h"

namespace aislinn
{

/**
 * The VertexInput classes let the application developer specify how to feed input to a renderjob:
 *          How the task and its shaders will read information (configuration):
 *              Does this task (and its shaders) expect 2D positions? 3D positions? normals? is it indexed? etc.)
 *          Where to find the input and how often to draw each set of buffers (instances* of the configuration):
 *              What buffers to use, in what combination
 *
 *      * note the use of instance, it's the NON-graphics interpretation.
 *
 * The main responsibility is keeping all that information together, ensure its correctness and uniformly providing it
 * to the task. It ties buffer interpration, buffer binding and shader attributes together in one cohesive unit.
 *
 * Key features of this module:
 *      It supports vertex input consisting of multiple buffers.
 *      It supports multiple buffer sets being interpreted with the same vertex input description.
 *      It does not take ownership of the buffers, so that a buffer could potentially be re-used in a different configuration.
 *      It checks whether the current VertexInput is valid for rendering.
 *
 * Configuration model:
 *      The VertexInputConfiguration has a list of VertexInputBufferDescriptors.
 *      Each VertexInputBufferDescriptor describes a buffer that will be part of the VertexInput, and how to interpret it.
 *      Additionally, each VertexInputBufferDescriptor has a series of ShaderAttributeDescriptors.
 *      The ShaderAttributeDescriptor is a part of the interpretation:
 *          it explains what shader attributes to read from this buffer.
 *          ShaderAttributes are defind in terms of the typical GLSL system, which works with locations.
 *          As such, you have to identify the shader attribute by their location.
 *
 * An example:
 *  {6 * sizeof(float), {1, VK_FORMAT_R32G32B32_SFLOAT, 0}, {2, VK_FORMAT_R32G32_SFLOAT, 3}},
 *  {2 * sizeof(uint32_t), {0, VK_FORMAT_R32G32_UINT, 0}}
 *
 *  This example reads from two buffers, the first one containing six floats per element, and connecting it to two shader attributes.
 *      Note that the second shader attribute reads only two floats. This means one float per element in the buffer goes unused.
 *  The second buffer contains two unsigned integers per element, which are connected to a single shader attribute (at location 0).
 */

// This class represents a single set of buffers, with additional drawing information (instancing, indexing, element count).
// Note that this class, currently, does not support the usual, full flexibility of draw calls in term of offsets and instance/vertex variables.
// That's okay for now.
class VertexInputBufferSet
{
public:
    // empty buffer set
    VertexInputBufferSet() = default;

    // these constructors allow the following syntax for vertex input creation:
    //     aislinn::VertexInputBufferSet vertexInputBufferSet(geometry::quad::indices.size(), indexBuffer, {vertexBuffer, texCoordBuffer});
    VertexInputBufferSet(uint32_t elementCount,
                         std::initializer_list<std::reference_wrapper<const Buffer>> buffers);

    VertexInputBufferSet(uint32_t elementCount,
                         const Buffer& indexBuffer,
                         std::initializer_list<std::reference_wrapper<const Buffer>> buffers);

    void buildCommandBuffer(VkCommandBuffer commandBuffer, uint32_t instanceCount) const;

private:
    VertexInputBufferSet(uint32_t elementCount, const Buffer* indexBuffer, 
						 Span<const std::reference_wrapper<const Buffer>> buffers);

    uint32_t m_elementCount = 0;
    const Buffer* m_indexBuffer = nullptr;
    std::vector<VkBuffer> m_dataBuffers = {};
    std::vector<VkDeviceSize> m_offsets = {};

    // I felt weird about using the friend keyword, as I was taught it was bad:
    //  "strong coupling!", "breaks the interface!", "ruins your encapsulation!"
    // which, yes, it does all of those things. After reading this, however:
    //  https://stackoverflow.com/questions/6718209/when-should-you-use-friend-classes
    // I think this is a valid use case: I do not want more details of this class exposed in its public api.
    //      (i.e. private is too strong, public is too weak)
    // The classes (Configuration and this one) are already strongly coupled.
    // Essentially, this way the public api is as if there is only one class, *BUT* buffer sets could be coupled
    //      with multiple vertex input configurations.
    // In the end, think of these classes as exactly that: they should be one class, but I want one part to have a
    //      different multiplicity, and be able to re-use one part, *without* exposing a more revealing public API.
    // The idea that they should/could be one class implies and accepts strong coupling, which is why they are part of
    //       the same file.
    friend class VertexInputConfiguration;
};

// This class represents the configuration part of the above explanation (see configuration model).
// It ties together buffer interpretation, shader attribute binding and topology information.
// A task should have one of these, defining how the task will interpret its inputs.
class VertexInputConfiguration
{
public:
    // User-facing api:
    struct ShaderAttributeDescriptor
    {
        uint32_t m_location;
        VkFormat m_format;
        uint32_t m_offset;
    };

    struct VertexInputBufferDescriptor
    {
        VertexInputBufferDescriptor() = default;
        VertexInputBufferDescriptor(uint32_t elementSizeInBytes, std::initializer_list<ShaderAttributeDescriptor> descriptors);
        VertexInputBufferDescriptor(uint32_t elementSizeInBytes, ShaderAttributeDescriptor descriptor);

        uint32_t m_elementSizeInBytes;
        std::vector<ShaderAttributeDescriptor> m_shaderAttributes;
    };

    VertexInputConfiguration() = default;
    VertexInputConfiguration(std::initializer_list<VertexInputBufferDescriptor> descriptors,
                             VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                             bool primitiveRestart = false);
    VertexInputConfiguration(Span<VertexInputBufferDescriptor> descriptors,
                             VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                             bool primitiveRestart = false);

    void addDescriptor(VertexInputBufferDescriptor descriptor);
    void addDescriptor(uint32_t elementSizeInBytes, std::initializer_list<ShaderAttributeDescriptor> shaderAttributes);
    void addDescriptors(std::initializer_list<VertexInputBufferDescriptor> descriptors);
    void addDescriptors(Span<const VertexInputBufferDescriptor> descriptors);

    // check if a given buffer set
    bool isValidBufferSet(const VertexInputBufferSet& bufferSet) const;

    // These functions are mostly used in aislinn internally.
    Span<const VkVertexInputBindingDescription> vertexInputBindingDescriptions() const;
    Span<const VkVertexInputAttributeDescription> vertexInputAttributeDescription() const;
    // Get Vk structs that represent the current state of the configuration object.
    VkPipelineVertexInputStateCreateInfo vertexInputStateCreateInfo() const;
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo() const;

private:
    // empty by default
    std::vector<VkVertexInputBindingDescription> m_vertexInputBindingDescriptions;
    std::vector<VkVertexInputAttributeDescription> m_vertexInputAttributeDescriptions;
    // default: triangle list without primitive restart.
    VkPrimitiveTopology m_topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    bool m_primitiveRestart = false;
    // The VkPipelineVertexInputStateCreateInfo and VkPipelineInputAssemblyStateCreateInfo
    //      are no longer *kept* in this class, as it created a fairly serious infraction against the DRY principle.
    //      example: if an instance of this class was moved, the std::vector were emptied but the structs were not updated.
};

}

#endif //AISLINN_VERTEXINPUT_H
