//
// Created by pieterjan on 16/11/2021.
//

#ifndef AISLINN_GEOMETRY_GEOMETRYMANAGER_H
#define AISLINN_GEOMETRY_GEOMETRYMANAGER_H

#include "aislinnutilities.h"
#include "aislinn/typedefs.h"
#include "vertexinput.h"

#include <vector>

namespace aislinn
{

class GeometryManager;
typedef int MeshID;

/* Currently we force a specific layout for the mesh, although it would be nice to change this in the future. */
struct MeshData
{
	Span<const float> m_positions;
	Span<const float> m_uvs;
	Span<const unsigned int> m_indices;
};

// Lightweight class representing the user API for geometry.
// RenderMesh should be movable, not copyable (because of observer model  of the data!)
class RenderMesh : NonCopyableButMovable
{
public:
	RenderMesh() = default;
	RenderMesh(GeometryManager * manager, MeshID uid); // this needs some specification for the data

	// move operations are allowed (and marked noexcept)
	RenderMesh(RenderMesh&&) noexcept;
	RenderMesh& operator=(RenderMesh&&) noexcept;


	// The public API starts here (including the destructor: the user lets these go out of scope)
	~RenderMesh(); // Signal geometry manager when it goes out of scope

	// update the mesh data
	void updateData(MeshData newData);

	// Allow user to clean up any dependent resources; even if the handle stays within scope.
	void invalidate() noexcept;

	inline bool isValid() const {return m_geometryManager != nullptr;}
	inline MeshID uid() const {return m_uid;}

private:
	// Has a reference to a GeometryManager (observer model), and its own uid to identify itself  
	GeometryManager* m_geometryManager = nullptr; // raw pointer NEVER implies ownership
	MeshID m_uid = -1;

};


// Internal class managing the geometry on both CPU and GPU side. 
class GeometryManager : public NonCopyableButMovable
{
public:
	// Constructors
	GeometryManager();
	// We allow moving in this class, but *only* if it doesn't leave any dangling references.
    // For now, this happens through asserts. Essentially, if you move a live MemoryBank, the program will crash (instead of letting you use dangling pointers).
	// It's to allow moving of any containing classes while avoiding dangling references.
	GeometryManager(GeometryManager&& other) noexcept;
	GeometryManager& operator=(GeometryManager&& other) noexcept;

	// creating meshes API (public user api)
	// GeometryManager needs to make a (processed) copy of the data.
	RenderMesh CreateRenderMesh(MeshData data);
	void updateRenderMesh(MeshID meshID, MeshData data);

	// GPU streaming API (private, internal api)
	void ensureLoaded(VkDevice, aislinn::val::DeviceMemoryBank&, MeshID meshId);

	aislinn::VertexInputBufferSet meshBufferSet(MeshID meshID);

	void cleanUp(MeshID meshId) noexcept;

private:
	// store all information about the existing render meshes
	struct MeshRecord
	{
		bool isUsed;		// Whether this Mesh is still in use.
		bool isOnGPU;		// Whether this Mesh has data on the GPU. 
		bool isConsistent;  // Whether the on-GPU data is consistent (i.e. has the Mesh changed since it was last uploaded.)
		int  vertexCount;
		int  indexCount;
		std::vector<float> positionData;
		std::vector<float> uvData;
		std::vector<unsigned int> indexData;

		aislinn::Buffer positionBuffer;
		aislinn::Buffer uvBuffer;
		aislinn::Buffer indexBuffer;
		aislinn::VertexInputBufferSet bufferSet;
	};

	std::vector<MeshRecord> m_meshRecords; // TODO: this absolutely needs to change -- but let's make it work first
};

}

#endif 