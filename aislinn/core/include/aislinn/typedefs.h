//
// Created by pieterjan on 01/01/2021.
//

#ifndef AISLINN_TYPEDEFS_H
#define AISLINN_TYPEDEFS_H

#include "val.h"

namespace aislinn
{
    // TODO: add additional functionality to Buffer and Image. DRY.
    using Buffer = val::Buffer;
    using Image = val::Image;
}

#endif //AISLINN_TYPEDEFS_H
