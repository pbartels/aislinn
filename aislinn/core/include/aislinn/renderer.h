//
// Created by pieterjan on 24/11/2021.
//

#ifndef AISLINN_RENDERER_H
#define AISLINN_RENDERER_H

#include "val.h"

#include "aislinnutilities.h"
#include "geometry/geometrymanager.h"
#include "geometry/renderinstance.h"
#include "rendertask.h"


namespace aislinn
{

// This is the real API for creating a surface!
using SurfaceInitializer = aislinn::val::SurfaceInitializer; 

// Main interface to the Aislinn api
class Renderer : public NonCopyableButMovable
{
public:
	// TODO: Configuration parameter -> configure vulkan backend
	Renderer(SurfaceInitializer surfaceInitializer);

	// No other constructor needed, nor a destructor, because of RAII in val 

	// Geometry
	RenderMesh createRenderMesh(MeshData meshData);

	// Render functionality
	void render(Span<const RenderInstance> instances);


private:
	void logSurfaceDetails() const;
	bool validateSurface() const;

	// vulkan objects (through val) - RAII - these can be gathered into a VulkanContext or something?
	// All of this needs to be refactored! -- based on good Vulkan usage and understanding.
	aislinn::val::VulkanBackEnd m_vkBackend;
	aislinn::val::DeviceMemoryBank m_memoryBank;
	aislinn::val::QueueHandle m_queue; // currently we have hard-coded a configuration that has one queue only.
	aislinn::val::MovableVkCommandPool m_commandPool; // This is only really a member because it needs to stay around while the command buffer is in use. RAII.
	aislinn::val::MovableVkCommandBuffer m_commandBuffer; // currently re-using only one command buffer (which is a bit dumb)

	// Presentation stuff
	void initializeSwapchain();
	aislinn::val::MovableVkSurface m_surface;
	aislinn::val::MovableVkSwapchain m_swapchain;
	std::array<aislinn::val::MovableVkImageView, 3> m_swapchainImageViews;

	// GeometryManager
	GeometryManager m_geometryManager;

	// The render task
	RenderTask m_renderTask;
	
};


}


#endif 