//
// Created by pieterjan on 04/01/2021.
//

#ifndef AISLINN_RENDERTASK_H
#define AISLINN_RENDERTASK_H

#include <vector>

#include "vulkan/vulkan.h"

#include "val.h"
#include "aislinn/geometry/renderinstance.h"

namespace aislinn
{

/**
 * This class represents a single render task in the render graph.
 * It draws vertex input into an image through one or more draw calls.
 */
class RenderTask
{
public:
    RenderTask() = default;
    RenderTask(VkDevice vulkanDevice,
			   aislinn::val::DeviceMemoryBank& memoryBank,
			   aislinn::GeometryManager& geometryManager,	
               VkFormat format,
               uint32_t windowWidth,
               uint32_t windowHeight,
               Span<aislinn::val::MovableVkImageView> targetImageViews);

	bool valid() const;

    void buildCommandBuffer(VkCommandBuffer commandBuffer,
                            size_t nextImageViewIndex,
							Span<const RenderInstance> instancesToRender,
                            float time) const;


private:
	// Task context:
    VkDevice m_device = VK_NULL_HANDLE;
    aislinn::val::DeviceMemoryBank* m_memoryBank = nullptr;
    aislinn::GeometryManager* m_geometryManager = nullptr;

    aislinn::val::MovableVkShaderModule m_vertShaderModule;
    aislinn::val::MovableVkShaderModule m_fragShaderModule;

    aislinn::val::MovableVkRenderPass m_renderpass;
    aislinn::val::MovableVkDescriptorPool m_descriptorPool;
    aislinn::val::MovableVkPipelineLayout m_pipelineLayout;
    aislinn::val::MovableVkGraphicsPipeline m_pipeline;

    std::vector<aislinn::val::MovableVkFramebuffer> m_framebuffers;

    VkClearValue m_clearValue;
    VkRect2D m_r2D;

};

}

#endif //AISLINN_RENDERTASK_H
