//
// Created by pieterjan on 06/11/2020.
//

#ifndef AISLINN_AISLINNUTILITIES_H
#define AISLINN_AISLINNUTILITIES_H

#include <gsl/span>

/**
 * Some general utilities, wrappers around names, etc.
 */
namespace aislinn
{
    // We use gsl::span to refer to a contiguous block of memory
    // it allows making abstraction of how, where and by who the data was allocated.
    // it does NOT take ownership of the data whatsoever.
    // it replaces the (T*, size_t) way of passing around data.
    // Note that you can assign a std::vector straight to a span, and it works nicely and easily.
    template<typename datatype> using Span = gsl::span<datatype>;

    template<typename Element, template <typename, typename...> class Container, typename... Args>
    Span<const std::byte> asBytes(const Container<Element, Args...>& data)
    {
        return gsl::as_bytes(Span<const Element>(data));
    }

    // Need this one for std::arrays
    template<typename Element, size_t N, template <typename, size_t> class Container>
    Span<const std::byte> asBytes(const Container<Element, N>& data)
    {
        return gsl::as_bytes(Span<const Element>(data));
    }

    class NonCopyableButMovable
    {
    protected:
        // protected constructor and destructor, only to be used as base class
        NonCopyableButMovable() = default;
        ~NonCopyableButMovable() = default;
        // move operations are allowed (and marked noexcept)
        NonCopyableButMovable(NonCopyableButMovable&&) noexcept = default;
        NonCopyableButMovable& operator=(NonCopyableButMovable&&) noexcept = default;
        // Copy is deleted
        NonCopyableButMovable(const NonCopyableButMovable&) = delete;
        NonCopyableButMovable operator=(const NonCopyableButMovable&) = delete;
    };

    class NonMovable
    {
    protected:
        // protected constructor and destructor, only to be used as base class
        NonMovable() = default;
        ~NonMovable() = default;
        // copy operations deleted
        NonMovable(const NonMovable&) = delete;
        NonMovable operator=(const NonMovable&) = delete;
        // move operations deleted
        NonMovable(NonMovable&&) noexcept = delete;
        NonMovable& operator=(const NonMovable&&) noexcept = delete;
    };

}

#endif //AISLINN_AISLINNUTILITIES_H
