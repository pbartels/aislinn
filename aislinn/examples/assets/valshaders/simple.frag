#version 450 core
#extension GL_KHR_vulkan_glsl : enable

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uvs;
layout (location = 2) in float instance;

layout (set = 0, binding = 0) uniform sampler2D uSampler;

layout (push_constant) uniform pushConstants
{
    layout(offset = 64) float time;
};

layout (location = 0) out vec4 fragColor;

void main() {
    vec4 test = texture(uSampler, uvs);
    //vec4 test = vec4(uvs, 0, 0);
    //fragColor = vec4(abs(cos(time)) * uvs.x, uvs.y, 0.5 * (sin(time) + 1), 1.0);
    fragColor = abs(sin(time - 0.5 * instance)) * test;
}
