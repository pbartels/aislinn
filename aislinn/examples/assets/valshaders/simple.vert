#version 450 core
#extension GL_KHR_vulkan_glsl : enable

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUvs;

layout(location = 0) out vec3 wsPosition;
layout(location = 1) out vec2 uvs;
layout(location = 2) out float instance;

layout (push_constant) uniform pushConstants
{
    mat4 mvpMatrix;
};

void main() {
    gl_Position = mvpMatrix * vec4(position, 1);
    wsPosition = gl_Position.xyz / gl_Position.w;
    uvs = vertexUvs;
    instance = float(gl_InstanceIndex);
}