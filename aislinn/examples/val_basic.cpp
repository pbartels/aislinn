//
// Created by pbartels on 8/29/20.
//

#include <iostream>
#include <string>
#include <vector>
#include <cstring>

#include "vulkan/vulkan.h"

// io includes
#include "io/file.h"
#include "io/image.h"
// data includes
#include "quad.h"
// glfw backend
#include "glfwbackend.h"
// aislinn::val
#include "val.h"

constexpr uint32_t windowHeight = 400;
constexpr uint32_t windowWidth = 400;

static std::array<float, 16> mvpMatrix = { 1, 0, 0, 0,
                                          0, 1, 0, 0,
                                          0, 0, 1, 0,
                                          0, 0, 0, 1 };

void updateMvpMatrix(float time)
{
    mvpMatrix[0] = 0.5 * cos(time);
    mvpMatrix[1] = 0.5 * sin(time);
    mvpMatrix[4] = -0.5 * sin(time);
    mvpMatrix[5] = 0.5 * cos(time);
}

int main()
{
    // Create vulkan back end
    aislinn::val::VulkanBackEnd vulkanBackend(aislinn::val::configfactory::validationConfig());
    vulkanBackend.logDetails();

    // grab the first queue created (in this config the only one)
    aislinn::val::QueueHandle aislinnQueue = vulkanBackend.queue(0);
    VkQueue queue = aislinnQueue.handle();

    // Create a memory bank to allow finding and using device memory
    aislinn::val::DeviceMemoryBank aislinnMemoryBank(vulkanBackend.vulkanPhysicalDevice(), vulkanBackend.vulkanDevice());


    aislinn::val::ImmovableVkCommandPool commandPool(aislinnQueue.device(),
        aislinn::val::vulkanstructs::commandPoolCreateInfo(aislinnQueue.family()));
    aislinn::val::ImmovableVkCommandBuffer commandBuffer(vulkanBackend.vulkanDevice(),
        aislinn::val::vulkanstructs::commandBufferAllocateInfo(commandPool.handle(), true));

    /** 3. Create a 'surface' **/
    glfwbackend::GlfwBackEnd backend("Aislinn test", windowWidth, windowHeight);
    aislinn::val::ImmovableVkSurface surface(vulkanBackend.vulkanInstance(), backend.surfaceInitializer());

    /** 3b. Check the surface's capabilities **/
    // Some of the following sections would ideally be checked against the surface's capabilities. This part shows how to get those.
    // check the surface's capabilities:
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vulkanBackend.vulkanPhysicalDevice(), surface.handle(), &surfaceCapabilities);
    std::cout << "The selected physical device has the following capabilities for the created surface:" << std::endl;
    std::cout << "\t Image count (min, max): " << surfaceCapabilities.minImageCount << ", "
        << surfaceCapabilities.maxImageCount << std::endl;
    std::cout << "\t Supported usage flags: " << surfaceCapabilities.supportedUsageFlags << std::endl;

    // Check the available imageFormats for the surface
    uint32_t surfaceFormatCount;
    // get the count first
    vkGetPhysicalDeviceSurfaceFormatsKHR(vulkanBackend.vulkanPhysicalDevice(), surface.handle(), &surfaceFormatCount, nullptr);
    std::vector<VkSurfaceFormatKHR> surfaceFormats(surfaceFormatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(vulkanBackend.vulkanPhysicalDevice(), surface.handle(), &surfaceFormatCount,
        surfaceFormats.data());
    std::cout << "\t Available surface formats: " << surfaceFormatCount << std::endl;
    for (VkSurfaceFormatKHR format : surfaceFormats)
    {
        std::cout << "\t\t" << "Format: " << format.format << ", Colorspace: " << format.colorSpace << std::endl;
    }

    /** 5. Uniform buffers (transforms) and textures **/

    /** 5b. Example texture **/
    // read in the image
    io::Image<uint8_t> albedo("aislinnExampleAssets/WoodFloor035_2K_Color.jpg", false,
        true);

    // Now we create a texture image
    aislinn::val::Image textureImage(vulkanBackend.vulkanDevice(), aislinnMemoryBank,
        aislinn::val::vulkanstructs::textureImageCreateInfo(albedo.width(), albedo.height()));

    VkImageSubresourceRange texImageSubresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT,
                                                       0, 1, 0, 1 };
    {
        // we need to a lot of work here: we create a staging image first, writing to the gpu memory directly
        aislinn::val::Image stagingImage(vulkanBackend.vulkanDevice(), aislinnMemoryBank,
            aislinn::val::vulkanstructs::stagingImageCreateInfo(albedo.width(), albedo.height()),
            albedo.data());

        // and then transfer from staging to texture, converting the format and layout
        aislinn::val::ImmovableVkCommandBuffer textureCommandBuffer(vulkanBackend.vulkanDevice(),
            aislinn::val::vulkanstructs::commandBufferAllocateInfo(commandPool.handle(), true));

        VkCommandBufferBeginInfo texCommandBufferBeginInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, nullptr,
                                                              VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
                                                              nullptr };
        vkBeginCommandBuffer(textureCommandBuffer.handle(), &texCommandBufferBeginInfo);

        // convert the staging image layout
        VkImageSubresourceRange imageSubresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT,
                                                         0, 1, 0, 1 };

        VkImageMemoryBarrier barriers[2];

        barriers[0] = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER, nullptr,
                       0, 0,
                       VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                       VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED,
                       stagingImage.handle(),
                       imageSubresourceRange };


        // transfer the texture image
        // we can re-use this
        barriers[1] = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER, nullptr,
                       0, 0,
                       VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                       VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED,
                       textureImage.handle(),
                       texImageSubresourceRange };

        vkCmdPipelineBarrier(textureCommandBuffer.handle(),
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
            0, (VkMemoryBarrier*) nullptr,
            0, (VkBufferMemoryBarrier*) nullptr,
            2, barriers);

        // now do the final image copy:
        VkImageSubresourceLayers imageSubresourceLayers = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };

        VkOffset3D texOffset = { 0, 0, 0 };
        VkExtent3D texExtent = { (uint32_t)albedo.width(), (uint32_t)albedo.height(), 1 };

        VkImageCopy imageCopy = { imageSubresourceLayers, texOffset, imageSubresourceLayers, texOffset, texExtent };

        vkCmdCopyImage(textureCommandBuffer.handle(),
            stagingImage.handle(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            textureImage.handle(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1, &imageCopy);

        VkImageMemoryBarrier textureToReadBarrier = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER, nullptr,
                                                     0, VK_ACCESS_SHADER_READ_BIT,
                                                     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                                     VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                                     VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED,
                                                     textureImage.handle(), texImageSubresourceRange };

        vkCmdPipelineBarrier(textureCommandBuffer.handle(),
            VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
            0, (VkMemoryBarrier*) nullptr,
            0, (VkBufferMemoryBarrier*) nullptr,
            1, &textureToReadBarrier);

        vkEndCommandBuffer(textureCommandBuffer.handle());

        VkSubmitInfo textureSubmitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr,
                                          0, nullptr, nullptr,
                                          1, textureCommandBuffer.pointer(),
                                          0, nullptr };
        vkQueueSubmit(queue, 1, &textureSubmitInfo, VK_NULL_HANDLE);
        // wait for texture loading to be done:
        vkQueueWaitIdle(queue);
    }

    // Create a image view
    aislinn::val::ImmovableVkImageView texImageView(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, nullptr, 0,
             textureImage.handle(), VK_IMAGE_VIEW_TYPE_2D,
             VK_FORMAT_R8G8B8A8_SRGB,
             {VK_COMPONENT_SWIZZLE_R,
              VK_COMPONENT_SWIZZLE_G,
              VK_COMPONENT_SWIZZLE_B,
              VK_COMPONENT_SWIZZLE_A},
             texImageSubresourceRange });

    // We also need a sampler:
    aislinn::val::ImmovableVkSampler texSampler(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO, nullptr, 0,
             VK_FILTER_NEAREST, VK_FILTER_NEAREST,
             VK_SAMPLER_MIPMAP_MODE_NEAREST,
             VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_ADDRESS_MODE_REPEAT, VK_SAMPLER_ADDRESS_MODE_REPEAT,
             0, VK_FALSE, 1, VK_FALSE, VK_COMPARE_OP_NEVER,
             0, 0, VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK, VK_FALSE });


    /** 6. Create the vertex data buffers **/
    aislinn::val::Buffer vertexBufferB(vulkanBackend.vulkanDevice(), aislinnMemoryBank, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, geometry::quad::positions);
    // double check this is still movable
    aislinn::val::Buffer vertexBuffer(std::move(vertexBufferB));
    aislinn::val::Buffer texCoordBuffer(vulkanBackend.vulkanDevice(), aislinnMemoryBank, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, geometry::quad::uvs);
    aislinn::val::Buffer indexBuffer(vulkanBackend.vulkanDevice(), aislinnMemoryBank, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, geometry::quad::indices);


    /** 7. Create a swapchain from the device and surface **/
    // For this, we need to enable the VK_KHR_swapchain extension when creating the logical device. See section 4.
    // The images in the swapchain will end up being your framebuffer.

    // We also need to check if the physical device supports the surface we've created before creating the swapchain
    // It's not a hard constraint but the validation layer complains if we don't
    VkBool32 supported;
    vkGetPhysicalDeviceSurfaceSupportKHR(vulkanBackend.vulkanPhysicalDevice(), 0, surface.handle(), &supported);
    if (supported == VK_FALSE)
    {
        std::cout << "The chosen queue family does not support the created surface." << std::endl;
        return 7;
    }

    aislinn::val::ImmovableVkSwapchain swapchain(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
             nullptr,
             0,
             surface.handle(),
             3,
             surfaceFormats[1].format,
             surfaceFormats[1].colorSpace,
             {windowWidth, windowHeight},
             1,
             VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
             VK_SHARING_MODE_EXCLUSIVE,
        // the next two fields are ignored with that sharing mode
        0,
        nullptr,
        VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
        VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        VK_PRESENT_MODE_FIFO_KHR,
        VK_TRUE,
        VK_NULL_HANDLE });

    // get the swapchain images
    uint32_t swapchainImageCount;
    vkGetSwapchainImagesKHR(vulkanBackend.vulkanDevice(), swapchain.handle(), &swapchainImageCount, nullptr);
    std::vector<VkImage> swapchainImages(swapchainImageCount);
    vkGetSwapchainImagesKHR(vulkanBackend.vulkanDevice(), swapchain.handle(), &swapchainImageCount, swapchainImages.data());
    std::cout << "Swapchain contains " << swapchainImageCount << " images." << std::endl;

    // Create image views for usage in the framebuffer (see below)
    std::vector<aislinn::val::MovableVkImageView> swapchainImageViews;
    for (uint32_t i = 0; i < swapchainImageCount; ++i)
    {
        swapchainImageViews.push_back({ vulkanBackend.vulkanDevice(),
                                       {VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                                   nullptr, 0,
                                   swapchainImages[i], VK_IMAGE_VIEW_TYPE_2D,
                                   surfaceFormats[1].format,
                                   {VK_COMPONENT_SWIZZLE_R,
                                    VK_COMPONENT_SWIZZLE_G,
                                    VK_COMPONENT_SWIZZLE_B,
                                    VK_COMPONENT_SWIZZLE_A},
                                   { VK_IMAGE_ASPECT_COLOR_BIT,
                                     0, 1,
                                     0, 1
                                   }} });
    }


    /** 8. Skipped for now: Create depth/stencil buffer **/

    /** 9. Create a renderpass. **/
    // this is mostly contained in chapter 7 of the vulkan programming guide.
    // This describes how the rendering will happen, and becomes part of the pipeline object.

    // First part in setting this up is describing the images involved.
    // For now we only have a color attachment that will serve solely as an output
    VkAttachmentDescription outputAttachmentDescription = { 0,
                                                           surfaceFormats[1].format, // making sure the format matches
                                                           VK_SAMPLE_COUNT_1_BIT, // Sample count
                                                           VK_ATTACHMENT_LOAD_OP_CLEAR, // load op: clear the framebuffer at the start
                                                           VK_ATTACHMENT_STORE_OP_STORE,
                                                           VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                                                           VK_ATTACHMENT_STORE_OP_DONT_CARE,
                                                           VK_IMAGE_LAYOUT_UNDEFINED,
                                                           VK_IMAGE_LAYOUT_PRESENT_SRC_KHR };

    /** 8. Skipped for now: Create depth/stencil buffer **/

    /** 9. Create a renderpass. **/
    // this is mostly contained in chapter 7 of the vulkan programming guide.
    // This describes how the rendering will happen, and becomes part of the pipeline object.

    // First part in setting this up is describing the images involved.
    // create a reference to it for use in the subpass (see below)
    VkAttachmentReference outputAttachmentReference = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

    // Next up we describe the subpasses.
    // Again, for now we have only one subpass that renders to our color attachment described above.
    VkSubpassDescription subpassDescription = { 0,
                                               VK_PIPELINE_BIND_POINT_GRAPHICS, // as opposed to compute
                                               0, nullptr, // input attachments
                                               1, &outputAttachmentReference, // output attachments
                                               nullptr, nullptr, // no need for these for now.
                                               0,
                                               nullptr }; // also not needed for now.

    aislinn::val::ImmovableVkRenderPass renderpass(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            nullptr,0, 1,
            &outputAttachmentDescription,
            1, &subpassDescription,
            0, nullptr });

    /** 10. Create the framebuffers **/
    // The framebuffers should correspond to the swapchain image views created earlier (section 7)
    // They also refer back to the renderpass we just created (section 9)
    // There is one framebuffer for each swapchain image view
    // Each framebuffer has one attachment per output attachment in the renderpass

    std::vector<aislinn::val::MovableVkFramebuffer> framebuffers;
    for (size_t i = 0; i < swapchainImageViews.size(); ++i)
    {
        framebuffers.push_back({ vulkanBackend.vulkanDevice(), {VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
                                                               nullptr, 0,
                                                               renderpass.handle(),
                                                               1, swapchainImageViews[i].pointer(),
                                                               windowWidth, windowHeight, 1} });
    }

    /** 11. Create the shader modules and shader stages **/

    // start with the vertex shader
    std::string vertShaderCode = io::readFile("valExampleShaders/simple.vert.spv");
    aislinn::val::ImmovableVkShaderModule vertShaderModule(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, nullptr, 0,
             vertShaderCode.size(), (uint32_t*)vertShaderCode.c_str() });
    // then do the fragment shader
    std::string fragShaderCode = io::readFile("valExampleShaders/simple.frag.spv");
    aislinn::val::ImmovableVkShaderModule fragShaderModule(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, nullptr, 0,
             fragShaderCode.size(), (uint32_t*)fragShaderCode.c_str() });

    // we then wrap these shaders into a shader stage creation info object.
    VkPipelineShaderStageCreateInfo shaderStages[2] = { {VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                                                         nullptr, 0,
                                                         VK_SHADER_STAGE_VERTEX_BIT,
                                                         vertShaderModule.handle(), "main", nullptr},
                                                       {VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                                                        nullptr, 0,
                                                        VK_SHADER_STAGE_FRAGMENT_BIT,
                                                        fragShaderModule.handle(), "main", nullptr} };

    /** 12. Create a graphics pipeline object **/
    // step 1: We need to feed in vertex input, which we describe through VertexInputState
    // Create a VertexInputBindingDescription
    VkVertexInputBindingDescription vertexInputBindingDescription[2] = { {0, 3 * sizeof(float), VK_VERTEX_INPUT_RATE_VERTEX},
        // we create a second one for the texture coordinates (reading two floats a time)
        {1, 2 * sizeof(float), VK_VERTEX_INPUT_RATE_VERTEX} };

    // and a VertexInputAttributeDescription
    // we read three floats from the buffer, feeding it into the shader attribute at location 0.
    // not stride or offset (set to zero)
    VkVertexInputAttributeDescription vertexInputAttributeDescription[2] = { {0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0},
                                                                            {1, 1, VK_FORMAT_R32G32_SFLOAT, 0} };


    VkPipelineVertexInputStateCreateInfo vertexInputState = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
                                                             nullptr, 0,
                                                             2, vertexInputBindingDescription,
                                                             2, vertexInputAttributeDescription };

    // step 2: Create an InputAssemblyStateCreateInfo
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
                                                                           nullptr, 0,
                                                                           VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                                                                           VK_FALSE };
    // step 3: create descriptor sets
    // we need a descriptor pool
    VkDescriptorPoolSize poolSize = { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1 };
    aislinn::val::ImmovableVkDescriptorPool descriptorPool(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO, nullptr, VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
            1, 1, &poolSize });

    // and layout bindings
    VkDescriptorSetLayoutBinding texSamplerSet = { 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1,
                                                  VK_SHADER_STAGE_FRAGMENT_BIT, nullptr };
    aislinn::val::ImmovableVkDescriptorSetLayout texLayout(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
           nullptr, 0,
           1, &texSamplerSet });

    VkDescriptorSetAllocateInfo	descriptorSetAllocateInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
                                                             nullptr,
                                                             descriptorPool.handle(),
                                                             1, texLayout.pointer() };

    aislinn::val::MovableVkDescriptorSets texDescriptorSet(vulkanBackend.vulkanDevice(), descriptorSetAllocateInfo);

    VkDescriptorImageInfo texDescriptorImageInfo = { texSampler.handle(), texImageView.handle(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };
    VkWriteDescriptorSet texDescriptorSetWrite = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, nullptr,
                                                  texDescriptorSet.handle(0), 0, 0, 1,
                                                  VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                                  &texDescriptorImageInfo, nullptr, nullptr };
    vkUpdateDescriptorSets(vulkanBackend.vulkanDevice(), 1, &texDescriptorSetWrite, 0, (VkCopyDescriptorSet*)nullptr);

    // step 4: create a viewport state
    VkViewport viewport = { 0, 0, (float)windowWidth, (float)windowHeight, 0.f, 1.f };
    VkRect2D scissor = { {0, 0}, {windowWidth, windowHeight} };


    VkPipelineViewportStateCreateInfo viewportStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
                                                                 nullptr, 0,
                                                                 1, &viewport,
                                                                 1, &scissor };

    // Define the rasterization state
    VkPipelineRasterizationStateCreateInfo rasterizationStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
                                                                           nullptr, 0,
                                                                           VK_FALSE, VK_FALSE,
                                                                           VK_POLYGON_MODE_FILL,
                                                                           VK_CULL_MODE_NONE,
                                                                           VK_FRONT_FACE_COUNTER_CLOCKWISE,
                                                                           VK_FALSE, 0, 0, 0,
                                                                           1.f };
    // create a color blend state
    VkPipelineColorBlendAttachmentState blendAttachmentState = { VK_FALSE,
                                                                VK_BLEND_FACTOR_SRC_COLOR,
                                                                VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR,
                                                                VK_BLEND_OP_ADD,
                                                                VK_BLEND_FACTOR_ONE,
                                                                VK_BLEND_FACTOR_ZERO,
                                                                VK_BLEND_OP_ADD,
                                                                VK_COLOR_COMPONENT_A_BIT
                                                                | VK_COLOR_COMPONENT_R_BIT
                                                                | VK_COLOR_COMPONENT_G_BIT
                                                                | VK_COLOR_COMPONENT_B_BIT };

    VkPipelineMultisampleStateCreateInfo multisampleStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
                                                                       nullptr, 0,
                                                                       VK_SAMPLE_COUNT_1_BIT,
                                                                       VK_FALSE,1,
                                                                       nullptr,
                                                                       VK_FALSE, VK_FALSE };

    VkPipelineColorBlendStateCreateInfo colorBlendStateCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
                                                                     nullptr, 0,
                                                                     VK_FALSE,
                                                                     VK_LOGIC_OP_COPY,
                                                                     1, &blendAttachmentState,
                                                                     {1, 1, 1, 1} };
    // Create a push constant range for a uniform (e.g. time)
    VkPushConstantRange uniforms[2] = { {VK_SHADER_STAGE_VERTEX_BIT, 0, 16 * sizeof(float)},
                                         {VK_SHADER_STAGE_FRAGMENT_BIT, 16 * sizeof(float), sizeof(float)} };

    aislinn::val::ImmovableVkPipelineLayout pipelineLayout(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
         nullptr, 0,
        1, texLayout.pointer(),
        2, uniforms });

    aislinn::val::ImmovableVkGraphicsPipeline pipeline(vulkanBackend.vulkanDevice(), { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
                                                                                 nullptr, 0,
                                                                                 2, &shaderStages[0],
                                                                                 &vertexInputState,
                                                                                 &inputAssemblyStateCreateInfo,
                                                                                 nullptr,
                                                                                 &viewportStateCreateInfo,
                                                                                 &rasterizationStateCreateInfo,
                                                                                 &multisampleStateCreateInfo,
                                                                                 nullptr,
                                                                                 &colorBlendStateCreateInfo,
                                                                                 nullptr,
                                                                                 pipelineLayout.handle(),
                                                                                 renderpass.handle(),
                                                                                 0,
                                                                                 VK_NULL_HANDLE,
                                                                                 0 });


    // Command buffer/render pass information that is the same every loop.
    VkClearColorValue clearColorValue = { 0, 0, 0, 1 };
    VkClearValue clearValue = { clearColorValue };
    VkOffset2D o2D = { 0, 0 };
    VkExtent2D e2D = { windowWidth, windowHeight };
    VkRect2D r2D = { o2D, e2D };
    VkDeviceSize offsets[1] = { 0 };
    VkCommandBufferBeginInfo commandBufferBeginInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                                                       nullptr,
                                                       0, // this means we can reuse it
                                                       nullptr };

    float time = 0.f;

    /** Render loop **/
    while (backend.keepRendering()) {

        // we create a semaphore to wait on the image to be ready.
        aislinn::val::ImmovableVkSemaphore imageReadySemaphore(vulkanBackend.vulkanDevice(),
            { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr, 0 });


        // get the index of the next buffer to use from the swapchain.
        uint32_t nextImageIndex;
        vkAcquireNextImageKHR(vulkanBackend.vulkanDevice(), swapchain.handle(), UINT64_MAX, imageReadySemaphore.handle(), VK_NULL_HANDLE, &nextImageIndex);

        // Create the command buffer
        // this implicitly resets the command buffer! See spec.
        vkBeginCommandBuffer(commandBuffer.handle(), &commandBufferBeginInfo);
        VkRenderPassBeginInfo renderpassBeginInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO, nullptr,
                                                     renderpass.handle(), framebuffers[nextImageIndex].handle(),
                                                     r2D, 1, &clearValue };

        vkCmdBeginRenderPass(commandBuffer.handle(), &renderpassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
        vkCmdBindPipeline(commandBuffer.handle(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.handle());
        vkCmdBindDescriptorSets(commandBuffer.handle(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout.handle(),
            0, 1, texDescriptorSet.pointer(), 0, nullptr);

        vkCmdPushConstants(commandBuffer.handle(), pipelineLayout.handle(), VK_SHADER_STAGE_VERTEX_BIT, 0, 16 * sizeof(float), mvpMatrix.data());
        vkCmdPushConstants(commandBuffer.handle(), pipelineLayout.handle(), VK_SHADER_STAGE_FRAGMENT_BIT, 16 * sizeof(float), sizeof(float), &time);

        vkCmdBindVertexBuffers(commandBuffer.handle(), 0, 1, vertexBuffer.pointer(), offsets);
        // this could be one call (with the one above) if the vertex buffers are put into an array instead:
        vkCmdBindVertexBuffers(commandBuffer.handle(), 1, 1, texCoordBuffer.pointer(), offsets);
        vkCmdBindIndexBuffer(commandBuffer.handle(), indexBuffer.handle(), 0, VK_INDEX_TYPE_UINT32);
        vkCmdDrawIndexed(commandBuffer.handle(), geometry::quad::indices.size(), 1, 0, 0, 0);

        vkCmdEndRenderPass(commandBuffer.handle());
        vkEndCommandBuffer(commandBuffer.handle());

        // wait until the framebuffer is available before writing into it.
        VkPipelineStageFlags bufferWrite = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        VkSubmitInfo submitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr,
                                   1, imageReadySemaphore.pointer(), &bufferWrite,
                                   1, commandBuffer.pointer(), 0, nullptr };

        aislinn::val::ImmovableVkFence renderFence(vulkanBackend.vulkanDevice(), { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr, 0 });

        vkQueueSubmit(queue, 1, &submitInfo, renderFence.handle());

        // Wait for rendering to be done.
        vkWaitForFences(vulkanBackend.vulkanDevice(), 1, renderFence.pointer(), VK_TRUE, UINT64_MAX);

        // then present
        VkPresentInfoKHR presentInfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, nullptr,
                                        0, nullptr,
                                        1, swapchain.pointer(),
                                        &nextImageIndex, nullptr };
        vkQueuePresentKHR(queue, &presentInfo);

        time += 0.05f;
        updateMvpMatrix(time);
    }

    /** Final. Clean up the objects we created. **/
    // RAII.

    return 0;
}
