//
// Created by pbartels on 8/29/20.
//

#include <iostream>
#include <string>
#include <vector>
#include <cstring>

#include "vulkan/vulkan.h"

// io includes
#include "io/file.h"
#include "io/image.h"
// data includes
#include "quad.h"
// glfw backend
#include "glfwbackend.h"
// aislinn includes
#include "aislinn.h"
#include "val.h"

constexpr uint32_t windowHeight = 800;
constexpr uint32_t windowWidth = 800;

int main()
{
    // Create vulkan back end
    aislinn::val::Config valConfig = aislinn::val::configfactory::validationConfig();
    aislinn::val::VulkanBackEnd vulkanBackend(valConfig);
    vulkanBackend.logDetails();

    // grab the first queue created (in this config the only one)
    aislinn::val::QueueHandle aislinnQueue = vulkanBackend.queue(0);
    VkQueue queue = aislinnQueue.handle();

    // Create a memory bank to allow finding and using device memory
    aislinn::val::DeviceMemoryBank aislinnMemoryBank(vulkanBackend.vulkanPhysicalDevice(), vulkanBackend.vulkanDevice());


    aislinn::val::ImmovableVkCommandPool commandPool(aislinnQueue.device(),
        aislinn::val::vulkanstructs::commandPoolCreateInfo(aislinnQueue.family()));
    aislinn::val::ImmovableVkCommandBuffer commandBuffer(vulkanBackend.vulkanDevice(),
        aislinn::val::vulkanstructs::commandBufferAllocateInfo(commandPool.handle(), true));

    /** 3. Create a 'surface' **/
    glfwbackend::GlfwBackEnd backend(valConfig.m_applicationName.c_str(), windowWidth, windowHeight);
    backend.logBackEndDetails();
    aislinn::val::ImmovableVkSurface surface(vulkanBackend.vulkanInstance(), backend.surfaceInitializer());

    /** 3b. Check the surface's capabilities **/
    // Some of the following sections would ideally be checked against the surface's capabilities. This part shows how to get those.
    // check the surface's capabilities:
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vulkanBackend.vulkanPhysicalDevice(), surface.handle(), &surfaceCapabilities);
    std::cout << "The selected physical device has the following capabilities for the created surface:" << std::endl;
    std::cout << "\t Image count (min, max): " << surfaceCapabilities.minImageCount << ", "
        << surfaceCapabilities.maxImageCount << std::endl;
    std::cout << "\t Supported usage flags: " << surfaceCapabilities.supportedUsageFlags << std::endl;

    // Check the available imageFormats for the surface
    uint32_t surfaceFormatCount;
    // get the count first
    vkGetPhysicalDeviceSurfaceFormatsKHR(vulkanBackend.vulkanPhysicalDevice(), surface.handle(), &surfaceFormatCount, nullptr);
    std::vector<VkSurfaceFormatKHR> surfaceFormats(surfaceFormatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(vulkanBackend.vulkanPhysicalDevice(), surface.handle(), &surfaceFormatCount,
        surfaceFormats.data());
    std::cout << "\t Available surface formats: " << surfaceFormatCount << std::endl;
    for (VkSurfaceFormatKHR format : surfaceFormats)
    {
        std::cout << "\t\t" << "Format: " << format.format << ", Colorspace: " << format.colorSpace << std::endl;
    }


    /** 7. Create a swapchain from the device and surface **/
    // For this, we need to enable the VK_KHR_swapchain extension when creating the logical device. See section 4.
    // The images in the swapchain will end up being your framebuffer.

    // We also need to check if the physical device supports the surface we've created before creating the swapchain
    // It's not a hard constraint but the validation layer complains if we don't
    VkBool32 supported;
    vkGetPhysicalDeviceSurfaceSupportKHR(vulkanBackend.vulkanPhysicalDevice(), 0, surface.handle(), &supported);
    if (supported == VK_FALSE)
    {
        std::cout << "The chosen queue family does not support the created surface." << std::endl;
        return 7;
    }

    aislinn::val::ImmovableVkSwapchain swapchain(vulkanBackend.vulkanDevice(),
        { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
             nullptr,
             0,
             surface.handle(),
             3,
             surfaceFormats[1].format,
             surfaceFormats[1].colorSpace,
             {windowWidth, windowHeight},
             1,
             VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
             VK_SHARING_MODE_EXCLUSIVE,
        // the next two fields are ignored with that sharing mode
        0,
        nullptr,
        VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
        VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        VK_PRESENT_MODE_FIFO_KHR,
        VK_TRUE,
        VK_NULL_HANDLE });

    // get the swapchain images
    uint32_t swapchainImageCount;
    vkGetSwapchainImagesKHR(vulkanBackend.vulkanDevice(), swapchain.handle(), &swapchainImageCount, nullptr);
    std::vector<VkImage> swapchainImages(swapchainImageCount);
    vkGetSwapchainImagesKHR(vulkanBackend.vulkanDevice(), swapchain.handle(), &swapchainImageCount, swapchainImages.data());
    std::cout << "Swapchain contains " << swapchainImageCount << " images." << std::endl;

    // Create image views for usage in the framebuffer (see below)
    std::vector<aislinn::val::MovableVkImageView> swapchainImageViews;
    for (uint32_t i = 0; i < swapchainImageCount; ++i)
    {
        swapchainImageViews.push_back({ vulkanBackend.vulkanDevice(),
                                       {VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                                   nullptr, 0,
                                   swapchainImages[i], VK_IMAGE_VIEW_TYPE_2D,
                                   surfaceFormats[1].format,
                                   {VK_COMPONENT_SWIZZLE_R,
                                    VK_COMPONENT_SWIZZLE_G,
                                    VK_COMPONENT_SWIZZLE_B,
                                    VK_COMPONENT_SWIZZLE_A},
                                   { VK_IMAGE_ASPECT_COLOR_BIT,
                                     0, 1,
                                     0, 1
                                   }} });
    }


    /** 6. Create the vertex data buffers **/
    aislinn::Buffer vertexBuffer(vulkanBackend.vulkanDevice(), aislinnMemoryBank, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, geometry::quad::positions);
    aislinn::Buffer texCoordBuffer(vulkanBackend.vulkanDevice(), aislinnMemoryBank, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, geometry::quad::uvs);
    aislinn::Buffer indexBuffer(vulkanBackend.vulkanDevice(), aislinnMemoryBank, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, geometry::quad::indices);


    aislinn::VertexInputConfiguration vertexInputConfiguration(
        { {3 * sizeof(float),{0, VK_FORMAT_R32G32B32_SFLOAT, 0}},
          {2 * sizeof(float),{1, VK_FORMAT_R32G32_SFLOAT, 0}}
        });


    aislinn::RenderTask renderTask(vertexInputConfiguration,
        vulkanBackend.vulkanDevice(),
        surfaceFormats[1].format,
        windowWidth, windowHeight,
        swapchainImageViews);

    renderTask.addVertexInputBufferSet({ (uint32_t)geometry::quad::indices.size(), indexBuffer,{vertexBuffer, texCoordBuffer}, 10 });


    float time = 0.f;

    /** Render loop **/
    while (backend.keepRendering()) {

        // we create a semaphore to wait on the image to be ready.
        aislinn::val::ImmovableVkSemaphore imageReadySemaphore(vulkanBackend.vulkanDevice(),
            { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, nullptr, 0 });


        // get the index of the next buffer to use from the swapchain.
        uint32_t nextImageIndex;
        vkAcquireNextImageKHR(vulkanBackend.vulkanDevice(), swapchain.handle(), UINT64_MAX, imageReadySemaphore.handle(), VK_NULL_HANDLE, &nextImageIndex);

        renderTask.buildCommandBuffer(commandBuffer.handle(),
            nextImageIndex,
            time);

        // wait until the framebuffer is available before writing into it.
        VkPipelineStageFlags bufferWrite = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        VkSubmitInfo submitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO, nullptr,
                                   1, imageReadySemaphore.pointer(), &bufferWrite,
                                   1, commandBuffer.pointer(), 0, nullptr };

        aislinn::val::ImmovableVkFence renderFence(vulkanBackend.vulkanDevice(), { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, nullptr, 0 });

        vkQueueSubmit(queue, 1, &submitInfo, renderFence.handle());

        // Wait for rendering to be done.
        vkWaitForFences(vulkanBackend.vulkanDevice(), 1, renderFence.pointer(), VK_TRUE, UINT64_MAX);

        // then present
        VkPresentInfoKHR presentInfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, nullptr,
                                        0, nullptr,
                                        1, swapchain.pointer(),
                                        &nextImageIndex, nullptr };
        vkQueuePresentKHR(queue, &presentInfo);

        time += 0.05f;
    }

    /** Aislinn uses RAII as a principle, so no clean up needed. **/

    return 0;
}
