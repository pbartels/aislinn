//
// Created by pbartels on 8/29/20.
//

#include <iostream>
#include <string>
#include <vector>
#include <cstring>

#include "vulkan/vulkan.h"

// io includes
#include "io/file.h"
#include "io/image.h"
// data includes
#include "quad.h"
// glfw backend
#include "glfwbackend.h"

// aislinn included
#include "aislinn.h"


int main()
{
    constexpr uint32_t windowHeight = 800;
    constexpr uint32_t windowWidth = 800;

    // What does it look like to use Aislinn?

    /** Windowing and input backend creation remains the application developer's problem. **/
    glfwbackend::GlfwBackEnd backend("Test", windowWidth, windowHeight);
    backend.logBackEndDetails();

    // AislinnRenderer needs to take a functor to create the vulkan surface. Application code should not contain any reference to Vulkan
    // So, instead of this:
    //aislinn::val::ImmovableVkSurface surface(vulkanBackend.vulkanInstance(), backend.surfaceInitializer());
    // We should set up a functor object that we can pass into the renderer constructor.
    //      This implies the Aislinn Renderer always needs a surface (maybe there could be an Aislinn::ComputePipeline class later on)
    // GlfwSurfaceCreator surfaceCreator;
    
    //Aislinn::Renderer aislinnRenderer(configParameter, surfaceCreator, ...); // is Renderer a good classname? How do we pass in the config, and what's the default one?
    aislinn::Renderer renderer(backend.surfaceInitializer());

    // Read in scene objects and create a RenderMesh for them
    aislinn::RenderMesh quadMesh = renderer.createRenderMesh({geometry::quad::positions, geometry::quad::uvs, geometry::quad::indices});
    aislinn::RenderInstance quadInstance(quadMesh, 5);



    // This kind of stuff remains the responsibility of the application, but it will only be relevant once the app can provide its own shaders. 
    //float time = 0.f;

    /** Render loop **/
    while (backend.keepRendering())
    {
        // There might be a call to aislinn here to start preparing the frame
        // In the future we might want to do some more intricate synchronization! Think this through!
        // Also, I should ensure proper synchronization with the CPU parts.
        // Ideally, the animation engine is calculating while the previous frame is rendering, etc.
        // For now, we are going to assume everything is done before the call to the renderer. 
        //aislinnRenderer.prepareFrame();

        // Here we do all sorts of things; 

        // Do the actual rendering
        // We want to pass in the new constants at some point; could be here or in the prepare call
        // the constants should be configurable eventually, but not initially.  
        renderer.render(aislinn::Span<const aislinn::RenderInstance>{std::addressof(quadInstance), 1});

        // Again, this eventually becomes the problem of the application.
        //time += 0.05f;

        // and finally, maybe there's a need for this:
        // aislinnRenderer.finalizeFrame();
    }

    // Obviously no clean up, as Aislinn is RAII based. 

    return 0;

}
