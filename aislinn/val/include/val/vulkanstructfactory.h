//
// Created by pieterjan on 18/12/2020.
//

#ifndef AISLINN_VULKANSTRUCTFACTORY_H
#define AISLINN_VULKANSTRUCTFACTORY_H

#include "vulkan/vulkan.h"

namespace aislinn::val::vulkanstructs
{
    // Command Pool and Buffer structs
    VkCommandPoolCreateInfo commandPoolCreateInfo(uint32_t queueFamily,
                                                  VkCommandPoolCreateFlags commandPoolCreateFlags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
    VkCommandBufferAllocateInfo commandBufferAllocateInfo(VkCommandPool commandPool, bool primary, uint32_t count = 1);

    // VkBufferCreateInfo
    // get a BufferCreateInfo primed for exclusive access, i.e. sharingMode == VK_SHARING_MODE_EXCLUSIVE
    VkBufferCreateInfo exclusiveBufferCreateInfo(VkBufferUsageFlags usageFlags, VkDeviceSize bufferSize);

    // VkImageCreateInfo
    // to use with a staging image, host visible, usage is transfer source
    VkImageCreateInfo stagingImageCreateInfo(uint32_t width, uint32_t height);
    // to use for a texture image, not host visible, usage is transfer dst and sampled.
    VkImageCreateInfo textureImageCreateInfo(uint32_t width, uint32_t height);
}

#endif //AISLINN_VULKANSTRUCTFACTORY_H
