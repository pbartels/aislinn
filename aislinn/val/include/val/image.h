//
// Created by pieterjan on 17/12/2020.
//

#ifndef AISLINN_IMAGE_H
#define AISLINN_IMAGE_H

#include "vulkan/vulkan.h"

#include "devicememory.h"
#include "vulkanabstractions.h"

/**
 * The image class represents a Vulkan Image Object (VkImage) that is successfully  bound to video memory.
 * It is part of the (conceptual) memory management module of Aislinn.
 * It provides ways of creating a memory-backed image, ways of writing into that memory (if it's host visible)
 * and ensures clean up of the memory on destruction, through its destructor as well as its usage of the MemoryBank class.
 * It contains all information to use the VkImage later on in VkPipeline objects/Descriptor sets.
 *
 * The class is lazy in the sense that it does not do anything outside of the responsibilities outlined above. e.g.:
 *          It does not check correctness of the memory usage explicitly (in terms of the usage flags etc.).
 */
namespace aislinn::val
{

class Image : private NonCopyableButMovable // movable to allow storage in containers
{
public:
    Image(VkDevice device, DeviceMemoryBank& memoryBank, VkImageCreateInfo imageCreateInfo, bool hostVisible=false);
    Image(VkDevice device, DeviceMemoryBank& memoryBank, VkImageCreateInfo imageCreateInfo, Span<const std::byte> data);

    template<typename Data>
    Image(VkDevice device, DeviceMemoryBank& memoryBank, VkImageCreateInfo imageCreateInfo, const Data& data) :
                            Image(device, memoryBank, imageCreateInfo, aislinn::asBytes(data))
    {}

    // no explicit destructor, as this is taken care of by the RAII wrapper
    // This class is *non-copyable*, but movable, because of the BufferHandleWrapper RAII wrapper member.
    // explicit defaults
    Image(Image&&) noexcept = default;
    Image& operator=(Image&&) noexcept = default;

    VkImage handle() const;
    const VkImage* pointer() const;
    VkMemoryRequirements memoryRequirements() const;

    bool hostVisible() const;

private:
    struct VulkanMemoryRequirements
    {
        VulkanMemoryRequirements(VkDevice device, VkImage image);

        VkMemoryRequirements m_vulkanMemoryRequirements;
    };

    MovableVkImage m_image;
    VulkanMemoryRequirements m_vulkanMemoryRequirements;
    MemoryBinding m_memoryBinding;
};

}

#endif //AISLINN_IMAGE_H
