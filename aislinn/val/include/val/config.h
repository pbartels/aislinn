//
// Created by pieterjan on 06/11/2020.
//

#ifndef AISLINN_CONFIG_H
#define AISLINN_CONFIG_H

#include <array>
#include <string>
#include <stdexcept>
#include <vector>

#include "vulkan/vulkan.h"

#include "aislinnutilities.h"

namespace aislinn::val
{

constexpr uint32_t nvidiaVendorID = 4318;

struct QueueConfig
{
    QueueConfig(VkQueueFlags queueRequirementFlags, uint32_t count, float priority);

    VkQueueFlags m_queueRequirements;
    uint32_t m_count;
    float m_priority;
};

/**
 * This struct represents all the user/developer options that can be chosen at run-time to instantiate Aislinn.
 * It is used as the constructor parameter of the VulkanBackend class.
 *
 * It's purely a data container, it does not perform any checking or validation. The fact that you correctly initialized
 * a Config object does not mean you will be able to successfully create a corresponding VulkanBackend. ("lazy design").
 */
struct Config
{
public:
    Config(const std::string & applicationName,
           uint32_t applicationVersion,
           uint32_t preferredVendor,
           VkPhysicalDeviceFeatures requestedDeviceFeatures,
           Span<const char*> vulkanInstanceExtensionsToUse,
           Span<const char*> vulkanInstanceLayersToUse,
           Span<const char*> vulkanDeviceExtensionsToUse,
           Span<const char*> vulkanDeviceLayersToUse,
           Span<QueueConfig> queueConfigs);

    const std::string m_applicationName;
    const uint32_t m_applicationVersion;
    const uint32_t m_preferredVendorID;

    const VkPhysicalDeviceFeatures m_requestedDeviceFeatures;

    const std::vector<const char*> m_vulkanInstanceExtensionsToUse;
    const std::vector<const char*> m_vulkanInstanceLayersToUse;
    const std::vector<const char*> m_vulkanDeviceExtensionsToUse;
    const std::vector<const char*> m_vulkanDeviceLayersToUse;
    const std::vector<QueueConfig> m_queueConfigs;
};

}

namespace aislinn::val::configfactory::definitions
{

    /**
     * There is a good reason these are defined right here in the header, as inline functions: I want it to very visible what settings are set in each case.
     */

    inline aislinn::val::Config windowsValidationConfig()
    {
        // Alternatively, you create this by hand
        // watch out: allocation vs initialization. This way we are sure they are all false.
        VkPhysicalDeviceFeatures requestedFeatures = {};

        std::array<const char*, 2> defaultVulkanInstanceExtensionsToUse = { "VK_KHR_surface", "VK_KHR_win32_surface" };
        std::array<const char*, 1> defaultVulkanInstanceLayersToUse = { "VK_LAYER_KHRONOS_validation" };
        std::array<const char*, 1> defaultVulkanDeviceExtensionsToUse = { "VK_KHR_swapchain" };
        std::array<const char*, 0> defaultVulkanDeviceLayersToUse = {};
        // by default, we make one queue that we use for all tasks.
        std::array<QueueConfig, 1> defaultQueueConfigs = { QueueConfig{(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT),
                                                                      1, 1.f} };
        return aislinn::val::Config("Aislinn - Windows - Validation", 1, nvidiaVendorID, 
                                    requestedFeatures,
                                    defaultVulkanInstanceExtensionsToUse,
                                    defaultVulkanInstanceLayersToUse,
                                    defaultVulkanDeviceExtensionsToUse,
                                    defaultVulkanDeviceLayersToUse,
                                    defaultQueueConfigs);
    }

    inline aislinn::val::Config windowsReleaseConfig()
    {
        // Alternatively, you create this by hand
        // watch out: allocation vs initialization. This way we are sure they are all false.
        VkPhysicalDeviceFeatures requestedFeatures = {};

        std::array<const char*, 2> defaultVulkanInstanceExtensionsToUse = { "VK_KHR_surface", "VK_KHR_win32_surface" };
        std::array<const char*, 0> defaultVulkanInstanceLayersToUse = {};
        std::array<const char*, 1> defaultVulkanDeviceExtensionsToUse = {"VK_KHR_swapchain"};
        std::array<const char*, 0> defaultVulkanDeviceLayersToUse = {};
        // by default, we make one queue that we use for all tasks.
        std::array<QueueConfig, 1> defaultQueueConfigs = { QueueConfig{(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT),
                                                                      1, 1.f} };
        return aislinn::val::Config("Aislinn - Windows - Release", 1, nvidiaVendorID, 
                                    requestedFeatures,
                                    defaultVulkanInstanceExtensionsToUse,
                                    defaultVulkanInstanceLayersToUse,
                                    defaultVulkanDeviceExtensionsToUse,
                                    defaultVulkanDeviceLayersToUse,
                                    defaultQueueConfigs);
    }

    inline aislinn::val::Config nixValidationConfig()
    {
        // Alternatively, you create this by hand
        // watch out: allocation vs initialization. This way we are sure they are all false.
        VkPhysicalDeviceFeatures requestedFeatures = {};
        // example of how to request a specific feature:
        //requestedFeatures.geometryShader = VK_TRUE;

        std::array<const char*, 2> defaultVulkanInstanceExtensionsToUse = { "VK_KHR_surface", "VK_KHR_xcb_surface" };
        std::array<const char*, 1> defaultVulkanInstanceLayersToUse = { "VK_LAYER_KHRONOS_validation" };
        std::array<const char*, 1> defaultVulkanDeviceExtensionsToUse = { "VK_KHR_swapchain" };
        std::array<const char*, 0> defaultVulkanDeviceLayersToUse = {};
        // by default, we make one queue that we use for all tasks.
        std::array<QueueConfig, 1> defaultQueueConfigs = { QueueConfig{(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT),
                                                                      1, 1.f} };
        return aislinn::val::Config("Aislinn - Nix - Validation", 1, nvidiaVendorID, 
                                    requestedFeatures,
                                    defaultVulkanInstanceExtensionsToUse,
                                    defaultVulkanInstanceLayersToUse,
                                    defaultVulkanDeviceExtensionsToUse,
                                    defaultVulkanDeviceLayersToUse,
                                    defaultQueueConfigs);
    }

    inline aislinn::val::Config nixReleaseConfig()
    {
        // Alternatively, you create this by hand
        // watch out: allocation vs initialization. This way we are sure they are all false.
        VkPhysicalDeviceFeatures requestedFeatures = {};

        std::array<const char*, 2> defaultVulkanInstanceExtensionsToUse = { "VK_KHR_surface", "VK_KHR_xcb_surface" };
        std::array<const char*, 0> defaultVulkanInstanceLayersToUse = {};
        std::array<const char*, 1> defaultVulkanDeviceExtensionsToUse = { "VK_KHR_swapchain" };
        std::array<const char*, 0> defaultVulkanDeviceLayersToUse = {};
        // by default, we make one queue that we use for all tasks.
        std::array<QueueConfig, 1> defaultQueueConfigs = { QueueConfig{(VK_QUEUE_COMPUTE_BIT | VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_TRANSFER_BIT),
                                                                      1, 1.f} };
        return aislinn::val::Config("Aislinn - Nix - Release", 1, nvidiaVendorID, 
                                    requestedFeatures,
                                    defaultVulkanInstanceExtensionsToUse,
                                    defaultVulkanInstanceLayersToUse,
                                    defaultVulkanDeviceExtensionsToUse,
                                    defaultVulkanDeviceLayersToUse,
                                    defaultQueueConfigs);
    }

    template<aislinn::val::Config windowsCreateOp(), 
             aislinn::val::Config nixCreateOp()>
    aislinn::val::Config selectConfigBasedOnPlatform()
    {
        // see: https://stackoverflow.com/questions/5919996/how-to-detect-reliably-mac-os-x-ios-linux-windows-in-c-preprocessor 
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
        return windowsCreateOp();
#elif defined(unix) || defined(__unix__) || defined(__unix)
        return nixCreateOp();
#else
        // Throwing a run time error in this function lets us still us VAL even when we cannot detect the platform. Think!
        throw std::logic_error("Compilation could not detect platform. Cannot use automatic Configuration detection.");
#endif
    }
}

namespace aislinn::val::configfactory 
{
    inline aislinn::val::Config validationConfig() {return definitions::selectConfigBasedOnPlatform<definitions::windowsValidationConfig, definitions::nixValidationConfig>();}
    inline aislinn::val::Config releaseConfig() {return definitions::selectConfigBasedOnPlatform<definitions::windowsReleaseConfig, definitions::nixReleaseConfig>();}
}

#endif //AISLINN_CONFIG_H
