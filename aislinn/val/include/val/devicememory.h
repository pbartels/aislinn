//
// Created by pieterjan on 05/11/2020.
//

#ifndef AISLINN_DEVICEMEMORY_H
#define AISLINN_DEVICEMEMORY_H

#include <vector>

#include "vulkan/vulkan.h"

#include "aislinnutilities.h"
#include "vulkanabstractions.h"

namespace aislinn::val
{

/**
 * This class abstracts away the device memory allocation for the developer.
 * It is a key part of the (conceptual) memory management module of Aislinn.
 * Its key responsibility is allocating, managing and freeing Device memory.
 *
 * This class should be usable without any other class in Aislinn (although this is not recommended, other parts of the
 *  library provide higher level abstractions), taking care of the memory management for you while letting you code in
 *  "pure vulkan" outside of the usage of this class.
 *
 * it manages various VkDeviceMemory objects, ensures clean up on destruction and provides a simple, uniform interface
 * for binding both VkBuffer and VkImage objects to the allocated VkDeviceMemory objects.
 *
 * It does *not* guarantee that separate buffers are in separate memory objects, in fact this is suboptimal and it is
 *  highly likely the MemoryBank will try to combine buffers/images into bigger regions. The memory used for a given
 *  buffer or image can be written to through the MemoryBinding objects handed out by this class.
 *  When the given MemoryBinding goes out of scope, it signals to the memory bank that the memory region becomes available again.
 *
 *  It is up to the application developer to ensure the memory bank lives longer than the MemoryBindings it hands out.
 *  It is up to the application developer to keep the MemoryBinding alive while the buffer/image is in use,
 *      otherwise the MemoryBank will reclaim the memory bound to the buffer.
 *
 *  The MemoryBinding object provide a clean, uniform interface for writing into host-visible memory. It hides the
 *  implementation of this functionality, ensuring that is correct throughout as long as the memory bank is implemented correctly.
 *
 * It does *not* manage the VkBuffer and VkImage objects. In this regards, this class is lazy.
 */
class DeviceMemoryBank : private NonCopyableButMovable
{
private:
    // This private internal class manages a single device memory region
    // It's main responsibility is keeping track of what regions of the memory are in use, and which are available.
    class MemoryStore : private NonCopyableButMovable
    {
    public:
        // RAII constructor
        MemoryStore(VkDevice device,
                    uint32_t memoryTypeIndex,
                    VkMemoryType memoryType,
                    VkDeviceSize size,
                    VkDeviceSize alignment);

        // no destructor needed because of raii wrapper!
        // non-copyable, movable because of raii wrapper
        MemoryStore(MemoryStore&&) noexcept = default;
        MemoryStore& operator=(MemoryStore&&) noexcept = default;
        
        VkDeviceSize size() const;
        VkDeviceSize alignment() const;
        uint32_t memoryTypeIndex() const;
        bool isHostVisible() const;

        VkDevice vulkanDevice() const;
        VkDeviceMemory memoryHandle() const;

        //VkDeviceSize freeSpace() const;

        //void bind(const VkImage& image);
        //void bind(const VkBuffer& buffer);

    private:
        uint32_t m_memoryTypeIndex;
        VkMemoryType m_memoryType;
        VkDeviceSize m_storeSize;
        VkDeviceSize m_alignment;
        MovableVkDeviceMemory m_deviceMemory;
    };

public:
    class MemoryBinding : private NonCopyableButMovable
    {
    public:
        MemoryBinding() = default;
        MemoryBinding(MemoryStore*, VkDeviceSize size, VkDeviceSize offset);
        ~MemoryBinding();

        // movable
        MemoryBinding(MemoryBinding&& other) noexcept;
        MemoryBinding& operator=(MemoryBinding&& other) noexcept;

        void load(Span<const std::byte> data);

        bool isHostVisible() const;

    private:
        bool valid() const;

        MemoryStore* m_memoryStore = nullptr;
        VkDeviceSize m_size = 0;
        VkDeviceSize m_offset = 0;
    };

public:
    DeviceMemoryBank(VkPhysicalDevice physicalDevice, VkDevice device);

    // NonCopyable through base interface

    // The destructor and move operations make sure we are not leaving dangling references.
    // For now, this happens through asserts. Essentially, if you move a live MemoryBank, the program will crash (instead of letting you use dangling pointers).
	// It's to allow moving of any containing classes while avoiding dangling references.
    DeviceMemoryBank(DeviceMemoryBank&& other) noexcept;
    DeviceMemoryBank& operator=(DeviceMemoryBank&& other) noexcept;

    MemoryBinding bind(VkBuffer buffer, VkMemoryRequirements requirements, bool hostVisible=false);
    MemoryBinding bind(VkImage image, VkMemoryRequirements requirements, bool hostVisible=false);

private:
    std::pair<uint32_t, VkMemoryType> getMemoryTypeIndex(VkMemoryRequirements memoryRequirements,
                                                         bool hostVisible);

    size_t findMemoryStore(VkMemoryRequirements memoryRequirements,
                           bool hostVisible);

    // Device information
    VkPhysicalDevice m_physicalDevice;
    VkDevice m_device;
    VkPhysicalDeviceMemoryProperties m_memoryProperties;

    // Memory stores
    std::vector<MemoryStore> m_memoryStores;
};

using MemoryBinding = DeviceMemoryBank::MemoryBinding;

}

#endif //AISLINN_DEVICEMEMORY_H
