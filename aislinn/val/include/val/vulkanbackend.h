//
// Created by pieterjan on 06/11/2020.
//

#ifndef AISLINN_VULKANBACKEND_H
#define AISLINN_VULKANBACKEND_H

#include "vulkan/vulkan.h"

#include "config.h"
#include "vulkanabstractions.h"

namespace aislinn::val
{

/**
 * Represents a handle for a queue. It's a handle for a queue, not a queue itself.
 * The difference is that when an instance of this class goes out of scope, no resource is destroyed.
 *
 * It's sole responsibility is to be a copyable object representing all information regarding a queue.
 * The availability of a default constructor implies that VK_NULL_HANDLE could be present.
 * It is up to the application developer to check if that happens, although a member function for validity is provided.
 */
class QueueHandle
{
public:
    /* Having a default constructor for copyable and/or movable types is a good idea (see core guidelines)
     * However it does imply a need for the programmer to check or ensure safety.
     *
     * The presence of a default constructor means the class has an invariant, stating that we never have a non-null
     *  handle to a queue while having a null device handle:
     *      assert(m_vulkanDevice != VK_NULL_HANDLE || m_vulkanQueue == VK_NULL_HANDLE);
     */
    QueueHandle() = default;
    QueueHandle(VkDevice device, uint32_t familyIndex, uint32_t queueIndex, VkQueue vulkanQueue);

    VkDevice device() const;
    uint32_t family() const;
    uint32_t queueIndex() const;
    VkQueue handle() const;

    // Check if this object is in a state that is valid for use in rendering.
    bool isValid() const;

private:
    const VkDevice m_vulkanDevice = VK_NULL_HANDLE;
    const uint32_t m_familyIndex = 0;
    const uint32_t m_queueIndex = 0;
    const VkQueue m_vulkanQueue = VK_NULL_HANDLE;
};

/**
 * Represents a Vulkan Backend (Instance and Logical Device) that is created from a Configuration.
 *
 * It does validation of the configuration, checks availability of requested extensions and layers etc.
 * It assumes that Aislinn users only ever need one Logical Device per Instance.
 *
 * Since queues are created together with the logical device, this class also manages the queues (and cleans them up when going out of scope).
 *
 */
class VulkanBackEnd : private NonCopyableButMovable
{
public:
    VulkanBackEnd(const Config& config);

    // No explicit destructor because the RAII is taken care of by the (private) wrappers.
    // Same goes for the move operator and constructor

    VkInstance vulkanInstance() const;
    VkPhysicalDevice vulkanPhysicalDevice() const;
    VkPhysicalDeviceProperties physicalDeviceProperties() const;
    // The logical device
    VkDevice vulkanDevice() const;
    // Queue information
    size_t queueCount() const;
    QueueHandle queue(size_t index) const;

    void logDetails() const;

private:
    // struct to store the combination of family and index in that family.
    struct QueueIndex
    {
        const uint32_t m_familyIndex;
        const uint32_t m_queueIndex;
    };

    void selectVulkanPhysicalDevice(uint32_t preferredVendorID);

    using InstanceWrapper = MovableVkInstance;
    using DeviceWrapper = MovableVkDevice;

    InstanceWrapper m_vulkanInstanceWrapper;
    VkPhysicalDevice m_vulkanPhysicalDevice;
    VkPhysicalDeviceProperties m_physicalDeviceProperties;
    std::vector<VkQueueFamilyProperties> m_queueFamilyProperties;
    DeviceWrapper m_vulkanDeviceWrapper;
    std::vector<QueueIndex> m_queueIndices;
};

}

#endif //AISLINN_VULKANBACKEND_H
