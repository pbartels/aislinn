//
// Created by pieterjan on 02/12/2020.
//

#ifndef AISLINN_VULKANABSTRACTIONS_H
#define AISLINN_VULKANABSTRACTIONS_H

#include "assert.h"
#include <functional>
#include <stdexcept>
#include <vector>

#include "vulkan/vulkan.h"

/**
 * This file defines several RAII wrappers around the various Vulkan objects.
 * These are the types to use when writing a barebones vulkan application with RAII replacing the create/allocate and destroy/free vulkan commands.
 *
 * That is their *only* intent, this code is lazy: it does not check for you if the freeing is a good idea, it just does.
 *  **That's the contract of a RAII wrapper, it frees what has been allocated*
 *
 * Consequently only use these when you are sure you want to free the allocated objects:
 *  for instance, it is not valid to free descriptor sets when the DescriptorPool was not created with the
 *  VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT set.
 *
 * All of these types have a default constructor, which makes them easier to use in collections etc.
 * It does mean that there is slightly more checking going on deletion. I am keeping this for now as I expect
 * moving/deleting will be not a bottleneck and this allows easier programming.
 * It does imply that all of these types have invariants regarding their handle combinations.
 *
 * Consider whether these are good for your use case or not, or maybe you might want to use some of them instead of all.
 * You can still call vkCreate/vkAllocate/vkDestroy/vkFree yourself for some or all of the vulkan objects.
 *
 * Most of the types defined here have some kind of invariant that should always be true.
 */

namespace aislinn::val::raiidefinitions
{
    /*
     * All of the classes defined here have class invariants regarding combinations of the vulkan handles they hold
     *
     * Make sure to keep these intact, and establish them in every constructor you write (Core guidelines E.5)
     * Write assertions for your assumptions (see The Pragmatic Programmer: "assertive programming")
     */
    template<bool movable>
    class VulkanInstanceRaiiWrapper
    {
    public:
        // This class currently does not have a class invariant, as the only handle could be either valid or null.

        // The default is nice when using them as a member, to allow for easy initialization.
        VulkanInstanceRaiiWrapper() = default;

        VulkanInstanceRaiiWrapper(VkInstanceCreateInfo createInfo)
        {
            VkResult result = vkCreateInstance(&createInfo, nullptr, &m_handle);
            // Make sure the instance creation was successful
            if(result != VK_SUCCESS) throw std::runtime_error("Vulkan instance creation failed.");
        }

        ~VulkanInstanceRaiiWrapper()
        {
            vkDestroyInstance(m_handle, nullptr);
        }

        // Non-Copyable
        VulkanInstanceRaiiWrapper(const VulkanInstanceRaiiWrapper&) = delete;
        VulkanInstanceRaiiWrapper operator=(const VulkanInstanceRaiiWrapper&) = delete;

        // Optionally movable
        template<bool moveExists=movable>
        VulkanInstanceRaiiWrapper(typename std::enable_if<moveExists && moveExists == movable,
                                                          VulkanInstanceRaiiWrapper &&>::type other) noexcept :
                m_handle(other.m_handle)
        {
            other.m_handle = VK_NULL_HANDLE;
        }
        // same but for move operator
        template<bool moveExists = movable>
        typename std::enable_if<moveExists && moveExists == movable,
                                VulkanInstanceRaiiWrapper &>::type operator=(VulkanInstanceRaiiWrapper &&other) noexcept
        {
            if(this != std::addressof(other))
            {
                // release our resource
                vkDestroyInstance(m_handle, nullptr);

                // steal from other
                m_handle = other.m_handle;

                // make sure other is no longer pointing to our resource.
                other.m_handle = VK_NULL_HANDLE;
            }
            return *this;
        }

        VkInstance handle() const
        {
            return m_handle;
        }

        const VkInstance* pointer() const
        {
            return &m_handle;
        }

    private:
        // C.45
        VkInstance m_handle = VK_NULL_HANDLE;
    };

    // RAII wrappers for logical device
    template<bool movable>
    class VulkanDeviceRaiiWrapper
    {
    public:
        // There is only one handle, that can be either null or valid, so no class invariant.

        // This is absolutely useless if the instantiation is NOT movable.
        // Deleting the code is hence unnecessary.
        VulkanDeviceRaiiWrapper() = default;

        VulkanDeviceRaiiWrapper(VkPhysicalDevice vulkanPhysicalDevice, VkDeviceCreateInfo createInfo)
        {
            VkResult result = vkCreateDevice(vulkanPhysicalDevice, &createInfo, nullptr, &m_handle);
            if(result != VK_SUCCESS) throw std::runtime_error("Logical device creation failed.");
        }

        ~VulkanDeviceRaiiWrapper()
        {
            if(m_handle != VK_NULL_HANDLE) vkDeviceWaitIdle(m_handle);
            vkDestroyDevice(m_handle, nullptr);
        }

        // Non copyable
        VulkanDeviceRaiiWrapper(const VulkanDeviceRaiiWrapper&) = delete;
        VulkanDeviceRaiiWrapper operator=(const VulkanDeviceRaiiWrapper&) = delete;

        // optionally movable
        template<bool moveExists=movable>
        VulkanDeviceRaiiWrapper(typename std::enable_if<moveExists && moveExists == movable,
                                              VulkanDeviceRaiiWrapper &&>::type other) noexcept :
                      m_handle(other.m_handle)
        {
            other.m_handle = VK_NULL_HANDLE;
        }

        // same but for operator
        template<bool moveExists=movable>
        typename std::enable_if<moveExists && moveExists == movable,
                                VulkanDeviceRaiiWrapper &>::type operator=(VulkanDeviceRaiiWrapper&& other) noexcept
        {
            if(this != std::addressof(other))
            {
                // clean up this resource
                if(m_handle != VK_NULL_HANDLE) vkDeviceWaitIdle(m_handle);
                vkDestroyDevice(m_handle, nullptr);

                // steal from other
                m_handle = other.m_handle;

                // make sure the other one no longer references our resource
                other.m_handle = VK_NULL_HANDLE;
            }
            return *this;
        }

        VkDevice handle() const {return m_handle;}
        const VkDevice* pointer() const {return &m_handle;}

    private:
        VkDevice m_handle = VK_NULL_HANDLE;
    };

    using SurfaceInitializer = std::function<void(VkInstance, VkSurfaceKHR*)>;

    template<bool movable>
    class VulkanSurfaceRaiiWrapper
    {
    public:
        /*
         * class invariant:
         *  either the surface handle is null or the instance handle is valid as well.
         *  assert(m_handle == VK_NULL_HANDLE || m_vulkanInstance != VK_NULL_HANDLE);
         */

        // The default is nice when using them as a member, to allow for easy initialization.
        // This is absolutely useless if the instantiation is NOT movable.
        // Deleting the code is hence unnecessary.
        VulkanSurfaceRaiiWrapper() = default;

        VulkanSurfaceRaiiWrapper(VkInstance instance, SurfaceInitializer surfaceInitializer) :
                                        m_vulkanInstance(instance),
                                        m_handle(VK_NULL_HANDLE)
        {
            surfaceInitializer(instance, &m_handle);
        }

        ~VulkanSurfaceRaiiWrapper()
        {
            assert(m_handle == VK_NULL_HANDLE || m_vulkanInstance != VK_NULL_HANDLE);

            if(m_vulkanInstance != VK_NULL_HANDLE)
                vkDestroySurfaceKHR(m_vulkanInstance, m_handle, nullptr);
        }

        // Non-Copyable
        VulkanSurfaceRaiiWrapper(const VulkanSurfaceRaiiWrapper &) = delete;

        VulkanSurfaceRaiiWrapper operator=(const VulkanSurfaceRaiiWrapper &) = delete;

        // Potentially movable
        template<bool moveExists = movable>
        VulkanSurfaceRaiiWrapper(
                typename std::enable_if<moveExists && moveExists == movable,
                                        VulkanSurfaceRaiiWrapper &&>::type other) noexcept :
                m_vulkanInstance(other.m_vulkanInstance), m_handle(other.m_handle)
        {
            // Make sure the other one does not delete our stolen resource
            other.m_handle = VK_NULL_HANDLE;
        }

        // same but for move operator
        template<bool moveExists = movable>
        typename std::enable_if<moveExists && moveExists == movable,
                                VulkanSurfaceRaiiWrapper &>::type operator=(VulkanSurfaceRaiiWrapper &&other) noexcept
        {
            if(this != std::addressof(other))
            {
                // release our resource
                if(m_vulkanInstance != VK_NULL_HANDLE)
                    vkDestroySurfaceKHR(m_vulkanInstance, m_handle, nullptr);

                // steal from other
                m_vulkanInstance = other.m_vulkanInstance;
                m_handle = other.m_handle;

                // make sure other is no longer pointing to our resource.
                other.m_handle = VK_NULL_HANDLE;
            }
            return *this;
        }

        VkSurfaceKHR handle() const
        {
            return m_handle;
        }

        const VkSurfaceKHR *pointer() const
        {
            return &m_handle;
        }

    private:
        // C.45
        VkInstance m_vulkanInstance = VK_NULL_HANDLE;
        VkSurfaceKHR m_handle = VK_NULL_HANDLE;
    };

    template<typename VulkanObject, typename VulkanObjectCreateInfo,
            VkResult VKAPI_CALL creationOp(VkDevice, const VulkanObjectCreateInfo *, const VkAllocationCallbacks *,
                                VulkanObject *),
            void VKAPI_CALL destructionOp(VkDevice, VulkanObject, const VkAllocationCallbacks *),
            bool movable = true>
    class DeviceDependentVulkanRaiiWrapper
    {
    public:
        /*
         * class invariant:
         *  either the object handle is null or the device handle is valid as well.
         *  assert(m_vulkanObjectHandle == VK_NULL_HANDLE || m_vulkanDevice != VK_NULL_HANDLE);
         */

        // It makes sense to have a default constructor for movable versions (e.g. when you want to have a list of things)
        // NonMovable types can still be default constructed but there is no real use,
        //  so I am not going to bother conditionally removing the code for it.
        // also nice for easier initialization in constructors.
        DeviceDependentVulkanRaiiWrapper() = default;

        DeviceDependentVulkanRaiiWrapper(VkDevice vulkanDevice,
                                         VulkanObjectCreateInfo createInfo) : m_vulkanDevice(vulkanDevice)
        {
            VkResult result = creationOp(vulkanDevice, &createInfo, nullptr, &m_vulkanObjectHandle);
            if(result != VK_SUCCESS) throw std::runtime_error("Resource acquisition failed");
        }

        ~DeviceDependentVulkanRaiiWrapper()
        {
            assert(m_vulkanObjectHandle == VK_NULL_HANDLE || m_vulkanDevice != VK_NULL_HANDLE);

            if(m_vulkanDevice != VK_NULL_HANDLE)
                destructionOp(m_vulkanDevice, m_vulkanObjectHandle, nullptr);
        }

        // This class template represents GPU resources, which should be non-copyable
        DeviceDependentVulkanRaiiWrapper(const DeviceDependentVulkanRaiiWrapper &) = delete;

        DeviceDependentVulkanRaiiWrapper operator=(const DeviceDependentVulkanRaiiWrapper &) = delete;

        // they can be optionally movable, using a template parameter and std::enable_if and SFINAE
        template<bool moveExists = movable>
        DeviceDependentVulkanRaiiWrapper(
                typename std::enable_if<moveExists && moveExists == movable,
                                        DeviceDependentVulkanRaiiWrapper &&>::type other) noexcept :
                m_vulkanDevice(other.m_vulkanDevice), m_vulkanObjectHandle(other.m_vulkanObjectHandle)
        {
            other.m_vulkanObjectHandle = VK_NULL_HANDLE;
        }

        // same but for move operator
        template<bool moveExists = movable>
        typename std::enable_if<moveExists && moveExists == movable,
                DeviceDependentVulkanRaiiWrapper &>::type
        operator=(DeviceDependentVulkanRaiiWrapper &&other) noexcept
        {
            if(this != std::addressof(other))
            {
                // release our resource
                if(m_vulkanDevice != VK_NULL_HANDLE)
                    destructionOp(m_vulkanDevice, m_vulkanObjectHandle, nullptr);

                // steal from other
                m_vulkanDevice = other.m_vulkanDevice;
                m_vulkanObjectHandle = other.m_vulkanObjectHandle;

                // make sure other is no longer pointing to our resource.
                other.m_vulkanObjectHandle = VK_NULL_HANDLE;
            }
            return *this;
        }

        VulkanObject handle() const
        {
            return m_vulkanObjectHandle;
        }

        const VulkanObject *pointer() const
        {
            return &m_vulkanObjectHandle;
        }

        inline VkDevice vulkanDevice() const
        {
            return m_vulkanDevice;
        }

    private:
        // Handle to the logical device, not owned by this class.
        // https://www.reddit.com/r/cpp/comments/8wbeom/coding_guideline_avoid_const_member_variables/
        // C.45
        VkDevice m_vulkanDevice = VK_NULL_HANDLE;
        VulkanObject m_vulkanObjectHandle = VK_NULL_HANDLE;
    };


    template<typename PooledObject, typename PooledObjectCreateInfo, typename PoolObject,
             VkResult VKAPI_CALL creationOp(VkDevice, const PooledObjectCreateInfo *, PooledObject*),
             typename DestructionReturn,
             DestructionReturn VKAPI_CALL destructionOp(VkDevice, PoolObject, uint32_t, const PooledObject*),
             PoolObject poolGetter(PooledObjectCreateInfo),
             uint32_t sizeGetter(PooledObjectCreateInfo),
             bool movable>
    class PooledVulkanObjectsWrapper
    {
    public:
        /*
         * class invariant:
         *  either the object has no pooled object, or both the device and pool handle are valid.
         *  assert(count() == 0 || (m_device != VK_NULL_HANDLE && m_pool != VK_NULL_HANDLE));
         */

        PooledVulkanObjectsWrapper() = default;

        PooledVulkanObjectsWrapper(VkDevice device,
                                   PooledObjectCreateInfo createInfo) :
                       m_device(device),
                       m_pool(poolGetter(createInfo)),
                       m_handles(sizeGetter(createInfo), VK_NULL_HANDLE)
        {
            VkResult result = creationOp(m_device, &createInfo, m_handles.data());
            if(result != VK_SUCCESS) throw std::runtime_error("Allocation from pool failed.");
        }

        ~PooledVulkanObjectsWrapper()
        {
            // class invariant: if the count is not zero, the handles are valid.
            assert(count() == 0 || (m_device != VK_NULL_HANDLE && m_pool != VK_NULL_HANDLE));
            if(count() > 0)
                destructionOp(m_device, m_pool, count(), m_handles.data());
        }

        PooledVulkanObjectsWrapper(const PooledVulkanObjectsWrapper&) = delete;
        PooledVulkanObjectsWrapper operator=(const PooledVulkanObjectsWrapper&) = delete;

        // SFINAE based move constructor.
        template<bool moveExists=movable>
        PooledVulkanObjectsWrapper(typename std::enable_if<moveExists && moveExists==movable, PooledVulkanObjectsWrapper&&>::type other) noexcept :
                                                                                  m_device(other.m_device),
                                                                                  m_pool(other.m_pool),
                                                                                  m_handles(std::move(other.m_handles))
        {
            // ensure the other doesn't cause any issues on deletion.
            assert(other.m_handles.empty());
        }

        // SFINAE based move operator
        template<bool moveExists=movable>
        typename std::enable_if<moveExists && moveExists==movable,
                                PooledVulkanObjectsWrapper&>::type operator=(PooledVulkanObjectsWrapper&& other) noexcept
        {
            if(this != std::addressof(other))
            {
                // release our resource
                assert(count() == 0 || (m_device != VK_NULL_HANDLE && m_pool != VK_NULL_HANDLE));
                if(count() > 0)
                    destructionOp(m_device, m_pool, count(), m_handles.data());

                // steal from other
                m_device = other.m_device;
                m_pool = other.m_pool;
                m_handles = std::move(other.m_handles);

                // make sure other does no harm
                assert(other.m_handles.empty());
            }
            return *this;
        }

        Span<const PooledObject> handles() const {return {m_handles.data(), m_handles.size()};}
        const PooledObject* pointer() const {return pointer(0);}
        const PooledObject* pointer(size_t idx) const {return m_handles.data() + idx;}
        PooledObject handle(size_t idx) const {return m_handles[idx];}
        size_t count() const {return m_handles.size();}
    private:
        VkDevice m_device = VK_NULL_HANDLE;
        PoolObject m_pool = VK_NULL_HANDLE;
        std::vector<PooledObject> m_handles;
    };

    template<typename PooledObject, typename PooledObjectCreateInfo, typename PoolObject,
            VkResult VKAPI_CALL creationOp(VkDevice, const PooledObjectCreateInfo *, PooledObject*),
            typename DestructionReturn,
            DestructionReturn VKAPI_CALL destructionOp(VkDevice, PoolObject, uint32_t, const PooledObject*),
            PoolObject poolGetter(PooledObjectCreateInfo),
            uint32_t sizeGetter(PooledObjectCreateInfo),
            bool movable>
    class PooledVulkanObjectWrapper
    {
    public:
        /*
         * class invariant:
         *  either the object has no pooled object, or both the device and pool handle are valid.
         *  assert(m_handle == VK_NULL_HANDLE || (m_device != VK_NULL_HANDLE && m_pool != VK_NULL_HANDLE));
         */

        PooledVulkanObjectWrapper() = default;

        PooledVulkanObjectWrapper(VkDevice device,
                                   PooledObjectCreateInfo createInfo) :
                m_device(device),
                m_pool(poolGetter(createInfo)),
                m_handle(VK_NULL_HANDLE)
        {
            assert(sizeGetter(createInfo) == 1);
            VkResult result = creationOp(m_device, &createInfo, &m_handle);
            if(result != VK_SUCCESS) throw std::runtime_error("Allocation from pool failed.");
        }

        ~PooledVulkanObjectWrapper()
        {
            assert(m_handle == VK_NULL_HANDLE || (m_device != VK_NULL_HANDLE && m_pool != VK_NULL_HANDLE));
            if(m_handle != VK_NULL_HANDLE) destructionOp(m_device, m_pool, 1, &m_handle);
        }

        PooledVulkanObjectWrapper(const PooledVulkanObjectWrapper&) = delete;
        PooledVulkanObjectWrapper operator=(const PooledVulkanObjectWrapper&) = delete;

        // SFINAE based move constructor.
        template<bool moveExists=movable>
        PooledVulkanObjectWrapper(typename std::enable_if<moveExists && moveExists==movable, PooledVulkanObjectWrapper&&>::type other) noexcept :
                                                                                m_device(other.m_device),
                                                                                m_pool(other.m_pool),
                                                                                m_handle(other.m_handle)
        {
            // ensure the other doesn't cause any issues on deletion.
            other.m_handle = VK_NULL_HANDLE;
        }

        // SFINAE based move operator.
        template<bool moveExists=movable>
        typename std::enable_if<moveExists && moveExists == movable, PooledVulkanObjectWrapper&>::type operator=(PooledVulkanObjectWrapper&& other) noexcept
        {
            if(this != std::addressof(other))
            {
                // release our resource
                assert(m_handle == VK_NULL_HANDLE || (m_device != VK_NULL_HANDLE && m_pool != VK_NULL_HANDLE));
                if(m_handle != VK_NULL_HANDLE) destructionOp(m_device, m_pool, 1, &m_handle);

                // steal from other
                m_device = other.m_device;
                m_pool = other.m_pool;
                m_handle = other.m_handle;

                // make sure other does no harm
                other.m_handle = VK_NULL_HANDLE;
            }
            return *this;
        }

        const PooledObject* pointer() const {return &m_handle;}
        PooledObject handle() const {return m_handle;}
    private:
        VkDevice m_device = VK_NULL_HANDLE;
        PoolObject m_pool = VK_NULL_HANDLE;
        PooledObject m_handle;
    };

    // define these ahead of time, keeping them in the raiidefinitions namespace
    inline VkCommandPool commandBufferPoolGetter(VkCommandBufferAllocateInfo info){return info.commandPool;};
    inline uint32_t commandBufferCountGetter(VkCommandBufferAllocateInfo info){return info.commandBufferCount;};
    inline VkDescriptorPool descriptorSetPoolGetter(VkDescriptorSetAllocateInfo info){return info.descriptorPool;};
    inline uint32_t descriptorSetCountGetter(VkDescriptorSetAllocateInfo info){return info.descriptorSetCount;};

    template<typename CreateInfoObject,
             VkResult VKAPI_CALL creationOp(VkDevice, VkPipelineCache, uint32_t, const CreateInfoObject*, const VkAllocationCallbacks*, VkPipeline*),
             bool movable>
    class VulkanPipelineRaiiWrapper
    {
    public:
        /*
         * Class invariant:
         *  if we have a valid handle to a pipeline, we should also have a non-null device handle.
         *  assert(m_handle == VK_NULL_HANDLE || m_device != VK_NULL_HANDLE);
         */
        VulkanPipelineRaiiWrapper() = default;

        VulkanPipelineRaiiWrapper(VkDevice device, CreateInfoObject createInfo, VkPipelineCache pipelineCache=VK_NULL_HANDLE) : m_device(device)
        {
            VkResult result = creationOp(m_device, pipelineCache, 1, &createInfo, nullptr, &m_handle);
            if(result != VK_SUCCESS) throw std::runtime_error("Pipeline creation failed.");
        }

        ~VulkanPipelineRaiiWrapper()
        {
            assert(m_handle == VK_NULL_HANDLE || m_device != VK_NULL_HANDLE);
            if(m_device != VK_NULL_HANDLE) vkDestroyPipeline(m_device, m_handle, nullptr);
        }

        VulkanPipelineRaiiWrapper(const VulkanPipelineRaiiWrapper&) = delete;
        VulkanPipelineRaiiWrapper operator=(const VulkanPipelineRaiiWrapper&) = delete;

        template<bool moveExists=movable>
        VulkanPipelineRaiiWrapper(typename std::enable_if<moveExists && moveExists==movable,
                                                          VulkanPipelineRaiiWrapper&&>::type other) : m_device(other.m_device), m_handle(other.m_handle)
        {
            // Ensure 'other' doesn't destroy our stolen resource.
            other.m_handle = VK_NULL_HANDLE;
        }

        template<bool moveExists=movable>
        typename std::enable_if<moveExists && moveExists==movable, VulkanPipelineRaiiWrapper&>::type operator=(VulkanPipelineRaiiWrapper&& other)
        {
            if(this != std::addressof(other))
            {
                // clean up whatever resource we are holding
                assert(m_handle == VK_NULL_HANDLE || m_device != VK_NULL_HANDLE);
                if(m_device != VK_NULL_HANDLE) vkDestroyPipeline(m_device, m_handle, nullptr);

                // steal from other
                m_device = other.m_device;
                m_handle = other.m_handle;

                // ensure other doesn't destroy our stolen resource
                other.m_handle = VK_NULL_HANDLE;
            }
            return *this;
        }

        VkPipeline handle() const {return m_handle;}
        const VkPipeline* pointer() const {return &m_handle;}
    private:
        VkDevice m_device = VK_NULL_HANDLE;
        VkPipeline m_handle = VK_NULL_HANDLE;
    };

    namespace notstd
    {
        template<class T>
        struct is_array : std::is_array<T> {};
        template<class T, std::size_t N>
        struct is_array<std::array<T, N>> : std::true_type {};
    };


    template<typename CreateInfoObject,
            VkResult VKAPI_CALL creationOp(VkDevice, VkPipelineCache, uint32_t, const CreateInfoObject*, const VkAllocationCallbacks*, VkPipeline*),
            bool movable, class Container = std::vector<VkPipeline>>
    class VulkanPipelinesRaiiWrapper
    {
    public:
        /*
         * Class invariant:
         *  if we have one or more valid handles to a pipeline, we should also have a non-null device handle.
         *  assert(m_size == 0 || m_device != VK_NULL_HANDLE);
         */
        VulkanPipelinesRaiiWrapper() = default;

        VulkanPipelinesRaiiWrapper(VkDevice device, Span<CreateInfoObject> createInfos, VkPipelineCache pipelineCache=VK_NULL_HANDLE) :
                                            m_device(device),
                                            m_size(createInfos.size())
        {
            resize();
            VkResult result = creationOp(m_device, pipelineCache, m_size, createInfos.data(), nullptr, m_handles.data());
            if(result != VK_SUCCESS) throw std::runtime_error("Pipeline creation failed.");
        }

        ~VulkanPipelinesRaiiWrapper()
        {
            assert(m_size == 0 || m_device != VK_NULL_HANDLE);
            if(m_device != VK_NULL_HANDLE)
            {
                // don't depend on size here, just call it for all of them.
                for(VkPipeline pipeline: m_handles) vkDestroyPipeline(m_device, pipeline, nullptr);
            }
        }

        VulkanPipelinesRaiiWrapper(const VulkanPipelinesRaiiWrapper&) = delete;
        VulkanPipelinesRaiiWrapper operator=(const VulkanPipelinesRaiiWrapper&) = delete;

        template<bool moveExists=movable>
        VulkanPipelinesRaiiWrapper(typename std::enable_if<moveExists && moveExists==movable,
                VulkanPipelinesRaiiWrapper&&>::type other) : m_device(other.m_device),
                                                             m_size(other.m_size),
                                                             m_handles(std::move(other.m_handles))
        {
            // Ensure 'other' doesn't destroy our stolen resource.
            other.m_size = 0;
            other.resize();
            other.m_handles.fill(VK_NULL_HANDLE);
        }

        template<bool moveExists=movable>
        typename std::enable_if<moveExists && moveExists==movable>::type operator=(VulkanPipelinesRaiiWrapper&& other)
        {
            if(this != std::addressof(other))
            {
                // clean up whatever resource we are holding
                assert(m_size == 0 || m_device != VK_NULL_HANDLE);
                if(m_device != VK_NULL_HANDLE)
                {
                    for(VkPipeline pipeline: m_handles) vkDestroyPipeline(m_device, m_handles, nullptr);
                }

                // steal from other
                m_device = other.m_device;
                m_handles = std::move(other.m_handles);

                // ensure other doesn't destroy our stolen resource
                other.m_size = 0;
                other.resize();
                other.m_handles.fill(VK_NULL_HANDLE);
            }
            return *this;
        }

        // number of valid handles in this object. The underlying container could have more space.
        size_t size() const {return m_size;}
        size_t maxSize() const {return m_handles.size();}
        VkPipeline handle(size_t idx) const {return m_handles[idx];}
        const VkPipeline* pointer(size_t idx) const {return m_handles.data() + idx;}
        const VkPipeline* pointer() const {return m_handles.data();}
    private:
        void resize()
        {
            if constexpr(!notstd::is_array<Container>::value)
            {
                m_handles.resize(m_size);
            }
        }

        VkDevice m_device = VK_NULL_HANDLE;
        size_t m_size;
        Container m_handles;
    };

}

/* TYPEDEFS */

namespace aislinn::val
{
    using MovableVkInstance = raiidefinitions::VulkanInstanceRaiiWrapper<true>;
    using ImmovableVkInstance = raiidefinitions::VulkanInstanceRaiiWrapper<false>;

    using MovableVkDevice = raiidefinitions::VulkanDeviceRaiiWrapper<true>;
    using ImmovableVkDevice = raiidefinitions::VulkanDeviceRaiiWrapper<false>;

	using SurfaceInitializer = raiidefinitions::SurfaceInitializer;
    using MovableVkSurface = raiidefinitions::VulkanSurfaceRaiiWrapper<true>;
    using ImmovableVkSurface = raiidefinitions::VulkanSurfaceRaiiWrapper<false>;

    using MovableVkBuffer = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkBuffer, VkBufferCreateInfo, vkCreateBuffer, vkDestroyBuffer>;
    using ImmovableVkBuffer = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkBuffer, VkBufferCreateInfo, vkCreateBuffer, vkDestroyBuffer, false>;

    using MovableVkImage = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkImage, VkImageCreateInfo, vkCreateImage, vkDestroyImage>;
    using ImmovableVkImage = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkImage, VkImageCreateInfo, vkCreateImage, vkDestroyImage, false>;

    using MovableVkCommandPool = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkCommandPool, VkCommandPoolCreateInfo, vkCreateCommandPool, vkDestroyCommandPool>;
    using ImmovableVkCommandPool = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkCommandPool, VkCommandPoolCreateInfo, vkCreateCommandPool, vkDestroyCommandPool, false>;

    using MovableVkDeviceMemory = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkDeviceMemory, VkMemoryAllocateInfo, vkAllocateMemory, vkFreeMemory>;
    using ImmovableVkDeviceMemory = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkDeviceMemory, VkMemoryAllocateInfo, vkAllocateMemory, vkFreeMemory, false>;

    using MovableVkImageView = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkImageView, VkImageViewCreateInfo, vkCreateImageView, vkDestroyImageView>;
    using ImmovableVkImageView = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkImageView, VkImageViewCreateInfo, vkCreateImageView, vkDestroyImageView, false>;

    using MovableVkSampler = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkSampler, VkSamplerCreateInfo, vkCreateSampler, vkDestroySampler>;
    using ImmovableVkSampler = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkSampler, VkSamplerCreateInfo, vkCreateSampler, vkDestroySampler, false>;

    using MovableVkPipelineLayout = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkPipelineLayout, VkPipelineLayoutCreateInfo, vkCreatePipelineLayout, vkDestroyPipelineLayout>;
    using ImmovableVkPipelineLayout = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkPipelineLayout, VkPipelineLayoutCreateInfo, vkCreatePipelineLayout, vkDestroyPipelineLayout, false>;

    using MovableVkDescriptorPool = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkDescriptorPool, VkDescriptorPoolCreateInfo, vkCreateDescriptorPool, vkDestroyDescriptorPool>;
    using ImmovableVkDescriptorPool = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkDescriptorPool, VkDescriptorPoolCreateInfo, vkCreateDescriptorPool, vkDestroyDescriptorPool, false>;

    using MovableVkDescriptorSetLayout = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkDescriptorSetLayout, VkDescriptorSetLayoutCreateInfo, vkCreateDescriptorSetLayout, vkDestroyDescriptorSetLayout>;
    using ImmovableVkDescriptorSetLayout = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkDescriptorSetLayout, VkDescriptorSetLayoutCreateInfo, vkCreateDescriptorSetLayout, vkDestroyDescriptorSetLayout, false>;

    using MovableVkRenderPass = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkRenderPass, VkRenderPassCreateInfo, vkCreateRenderPass, vkDestroyRenderPass>;
    using ImmovableVkRenderPass = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkRenderPass, VkRenderPassCreateInfo, vkCreateRenderPass, vkDestroyRenderPass, false>;

    using MovableVkShaderModule = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkShaderModule, VkShaderModuleCreateInfo, vkCreateShaderModule, vkDestroyShaderModule>;
    using ImmovableVkShaderModule = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkShaderModule, VkShaderModuleCreateInfo, vkCreateShaderModule, vkDestroyShaderModule, false>;

    using MovableVkFence = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkFence, VkFenceCreateInfo, vkCreateFence, vkDestroyFence>;
    using ImmovableVkFence = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkFence, VkFenceCreateInfo, vkCreateFence, vkDestroyFence, false>;

    using MovableVkSemaphore = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkSemaphore, VkSemaphoreCreateInfo, vkCreateSemaphore, vkDestroySemaphore>;
    using ImmovableVkSemaphore = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkSemaphore, VkSemaphoreCreateInfo, vkCreateSemaphore, vkDestroySemaphore, false>;

    using MovableVkFramebuffer = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkFramebuffer, VkFramebufferCreateInfo, vkCreateFramebuffer, vkDestroyFramebuffer>;
    using ImmovableVkFramebuffer = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkFramebuffer, VkFramebufferCreateInfo, vkCreateFramebuffer, vkDestroyFramebuffer, false>;

    using MovableVkSwapchain = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkSwapchainKHR, VkSwapchainCreateInfoKHR, vkCreateSwapchainKHR, vkDestroySwapchainKHR>;
    using ImmovableVkSwapchain = raiidefinitions::DeviceDependentVulkanRaiiWrapper<VkSwapchainKHR, VkSwapchainCreateInfoKHR, vkCreateSwapchainKHR, vkDestroySwapchainKHR, false>;

    using MovableVkCommandBuffers = raiidefinitions::PooledVulkanObjectsWrapper<VkCommandBuffer, VkCommandBufferAllocateInfo, VkCommandPool, vkAllocateCommandBuffers, void, vkFreeCommandBuffers,
                                                                               raiidefinitions::commandBufferPoolGetter, raiidefinitions::commandBufferCountGetter, true>;
    using ImmovableVkCommandBuffers = raiidefinitions::PooledVulkanObjectsWrapper<VkCommandBuffer, VkCommandBufferAllocateInfo, VkCommandPool, vkAllocateCommandBuffers, void, vkFreeCommandBuffers,
                                                                                raiidefinitions::commandBufferPoolGetter, raiidefinitions::commandBufferCountGetter, false>;
    using MovableVkCommandBuffer = raiidefinitions::PooledVulkanObjectWrapper<VkCommandBuffer, VkCommandBufferAllocateInfo, VkCommandPool, vkAllocateCommandBuffers, void, vkFreeCommandBuffers,
                                                                              raiidefinitions::commandBufferPoolGetter, raiidefinitions::commandBufferCountGetter, true>;
    using ImmovableVkCommandBuffer = raiidefinitions::PooledVulkanObjectWrapper<VkCommandBuffer, VkCommandBufferAllocateInfo, VkCommandPool, vkAllocateCommandBuffers, void, vkFreeCommandBuffers,
                                                                                raiidefinitions::commandBufferPoolGetter, raiidefinitions::commandBufferCountGetter, false>;

    using MovableVkDescriptorSets = raiidefinitions::PooledVulkanObjectsWrapper<VkDescriptorSet, VkDescriptorSetAllocateInfo, VkDescriptorPool, vkAllocateDescriptorSets, VkResult, vkFreeDescriptorSets,
                                                                                raiidefinitions::descriptorSetPoolGetter, raiidefinitions::descriptorSetCountGetter, true>;
    using ImmovableVkDescriptorSets = raiidefinitions::PooledVulkanObjectsWrapper<VkDescriptorSet, VkDescriptorSetAllocateInfo, VkDescriptorPool, vkAllocateDescriptorSets, VkResult, vkFreeDescriptorSets,
                                                                                  raiidefinitions::descriptorSetPoolGetter, raiidefinitions::descriptorSetCountGetter, false>;
    using MovableVkDescriptorSet = raiidefinitions::PooledVulkanObjectWrapper<VkDescriptorSet, VkDescriptorSetAllocateInfo, VkDescriptorPool, vkAllocateDescriptorSets, VkResult, vkFreeDescriptorSets,
                                                                              raiidefinitions::descriptorSetPoolGetter, raiidefinitions::descriptorSetCountGetter, true>;
    using ImmovableVkDescriptorSet = raiidefinitions::PooledVulkanObjectWrapper<VkDescriptorSet, VkDescriptorSetAllocateInfo, VkDescriptorPool, vkAllocateDescriptorSets, VkResult, vkFreeDescriptorSets,
                                                                                raiidefinitions::descriptorSetPoolGetter, raiidefinitions::descriptorSetCountGetter, false>;

    using MovableVkGraphicsPipeline = raiidefinitions::VulkanPipelineRaiiWrapper<VkGraphicsPipelineCreateInfo, vkCreateGraphicsPipelines, true>;
    using ImmovableVkGraphicsPipeline = raiidefinitions::VulkanPipelineRaiiWrapper<VkGraphicsPipelineCreateInfo, vkCreateGraphicsPipelines, false>;

    using MovableVkGraphicsPipelineVector = raiidefinitions::VulkanPipelinesRaiiWrapper<VkGraphicsPipelineCreateInfo, vkCreateGraphicsPipelines, true, std::vector<VkPipeline>>;
    using ImmovableVkGraphicsPipelineVector = raiidefinitions::VulkanPipelinesRaiiWrapper<VkGraphicsPipelineCreateInfo, vkCreateGraphicsPipelines, false, std::vector<VkPipeline>>;

    template<size_t N>
    using MovableVkGraphicsPipelineArray = raiidefinitions::VulkanPipelinesRaiiWrapper<VkGraphicsPipelineCreateInfo, vkCreateGraphicsPipelines, true, std::array<VkPipeline, N>>;
    template<size_t N>
    using ImmovableVkGraphicsPipelineArray = raiidefinitions::VulkanPipelinesRaiiWrapper<VkGraphicsPipelineCreateInfo, vkCreateGraphicsPipelines, false, std::array<VkPipeline, N>>;

    using MovableVkComputePipeline = raiidefinitions::VulkanPipelineRaiiWrapper<VkComputePipelineCreateInfo, vkCreateComputePipelines, true>;
    using ImmovableVkComputePipeline = raiidefinitions::VulkanPipelineRaiiWrapper<VkComputePipelineCreateInfo, vkCreateComputePipelines, false>;

    using MovableVkComputePipelineVector = raiidefinitions::VulkanPipelinesRaiiWrapper<VkComputePipelineCreateInfo, vkCreateComputePipelines, true, std::vector<VkPipeline>>;
    using ImmovableVkComputePipelineVector = raiidefinitions::VulkanPipelinesRaiiWrapper<VkComputePipelineCreateInfo, vkCreateComputePipelines, false, std::vector<VkPipeline>>;

    template<size_t N>
    using MovableVkComputePipelineArray = raiidefinitions::VulkanPipelinesRaiiWrapper<VkComputePipelineCreateInfo, vkCreateComputePipelines, true, std::array<VkPipeline, N>>;
    template<size_t N>
    using ImmovableVkComputePipelineArray = raiidefinitions::VulkanPipelinesRaiiWrapper<VkComputePipelineCreateInfo, vkCreateComputePipelines, false, std::array<VkPipeline, N>>;
}

#endif //AISLINN_VULKANABSTRACTIONS_H
