//
// Created by pieterjan on 24/11/2020.
//

#ifndef AISLINN_BUFFER_H
#define AISLINN_BUFFER_H

#include "vulkan/vulkan.h"

#include "devicememory.h"
#include "vulkanabstractions.h"

namespace aislinn::val
{

/**
 * The buffer class represents a Vulkan Buffer Object that is successfully bound to video memory.
 * It is part of the (conceptual) memory management module of Aislinn.
 * It provides ways of creating a memory-backed buffer, ways of writing into that memory (if it's host visible)
 * and ensures clean up of the memory on destruction, through its destructor as well as usage of the MemoryBank class.
 * It contains all information to use the VkBuffer later on in VkPipeline objects/Descriptor sets.
 *
 * The class is lazy in the sense that it does not do anything outside of the responsibilities outlined above. e.g.:
 *          It does not check correctness of the memory usage explicitly (in terms of the usage flags etc.).
 */
class Buffer : private NonCopyableButMovable
{
public:
    // Movable classes need a default constructor.
	Buffer() = default;

    Buffer(VkDevice device, DeviceMemoryBank& memoryBank, VkBufferCreateInfo bufferCreateInfo, bool hostVisible);
    Buffer(VkDevice device, DeviceMemoryBank& memoryBank, VkBufferUsageFlags usageFlags, Span<const std::byte> data);
    Buffer(VkDevice device, DeviceMemoryBank& memoryBank, VkBufferUsageFlags usageFlags, VkDeviceSize bufferSize, bool hostVisible=false);

    // This lets us pass any kind of data container to be loaded into video memory
    template<typename Data>
    Buffer(VkDevice device, DeviceMemoryBank& memoryBank, VkBufferUsageFlags usageFlags, const Data& data) :
                                Buffer(device, memoryBank, usageFlags, aislinn::asBytes(data))
    {}

    // no explicit destructor, as this is taken care of by the RAII wrapper
    // The same goes for the move and copy constructor.
    // This class is *non-copyable*, but movable, because of the BufferHandleWrapper RAII wrapper member.
    Buffer(Buffer&&) noexcept = default;
    Buffer& operator=(Buffer&&) noexcept = default;

    VkBuffer handle() const;
    const VkBuffer* pointer() const;
    VkMemoryRequirements memoryRequirements() const;

    bool hostVisible() const;

private:
    struct VulkanMemoryRequirements
    {
		VulkanMemoryRequirements() = default;
        VulkanMemoryRequirements(VkDevice device, VkBuffer buffer);

        VkMemoryRequirements m_vulkanMemoryRequirements = {};
    };

    // RAII wrapper, using the movable version
    MovableVkBuffer m_buffer;
    VulkanMemoryRequirements m_vulkanMemoryRequirements;
    MemoryBinding m_memoryBinding;
};

}

#endif //AISLINN_BUFFER_H
