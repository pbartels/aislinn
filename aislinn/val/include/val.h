//
// Created by pieterjan on 29/12/2020.
//

#ifndef AISLINN_VAL_H
#define AISLINN_VAL_H

#include "val/buffer.h"
#include "val/config.h"
#include "val/devicememory.h"
#include "val/image.h"
#include "val/vulkanabstractions.h"
#include "val/vulkanbackend.h"
#include "val/vulkanstructfactory.h"

#endif //AISLINN_VAL_H
