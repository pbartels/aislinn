//
// Created by pieterjan on 24/11/2020.
//

#include <memory>

#include "val/buffer.h"

// make sure includes are only included where they are truly needed.
#include "val/vulkanstructfactory.h"

using namespace aislinn::val;

Buffer::Buffer(VkDevice device, DeviceMemoryBank& memoryBank, VkBufferCreateInfo bufferCreateInfo, bool hostVisible) :
        m_buffer(device, bufferCreateInfo),
        m_vulkanMemoryRequirements(device, m_buffer.handle()),
        m_memoryBinding(memoryBank.bind(m_buffer.handle(),
                                        m_vulkanMemoryRequirements.m_vulkanMemoryRequirements,
                                        hostVisible))
{}

Buffer::Buffer(VkDevice device, DeviceMemoryBank& memoryBank, VkBufferUsageFlags usageFlags, Span<const std::byte> data) :
                            Buffer(device, memoryBank,
                                   vulkanstructs::exclusiveBufferCreateInfo(usageFlags, (VkDeviceSize) data.size()) ,true)
{
    m_memoryBinding.load(data);
}

Buffer::Buffer(VkDevice device, DeviceMemoryBank& memoryBank, VkBufferUsageFlags usageFlags, VkDeviceSize bufferSize, bool hostVisible) :
                            Buffer(device, memoryBank,
                                   vulkanstructs::exclusiveBufferCreateInfo(usageFlags, bufferSize), hostVisible)
{}

VkBuffer Buffer::handle() const
{
    return m_buffer.handle();
}

const VkBuffer* Buffer::pointer() const
{
    return m_buffer.pointer();
}

VkMemoryRequirements Buffer::memoryRequirements() const
{
    return m_vulkanMemoryRequirements.m_vulkanMemoryRequirements;
}

bool Buffer::hostVisible() const
{
    return m_memoryBinding.isHostVisible();
}

Buffer::VulkanMemoryRequirements::VulkanMemoryRequirements(VkDevice device, VkBuffer buffer)
{
    vkGetBufferMemoryRequirements(device, buffer, &m_vulkanMemoryRequirements);
}