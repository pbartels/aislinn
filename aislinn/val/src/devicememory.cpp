//
// Created by pieterjan on 05/11/2020.
//

#include <cstring>
#include <stdexcept>
#include <utility>

#include "val/devicememory.h"

using namespace aislinn::val;

// Raii constructor
DeviceMemoryBank::MemoryStore::MemoryStore(VkDevice device,
                                           uint32_t memoryTypeIndex,
                                           VkMemoryType memoryType,
                                           VkDeviceSize size,
                                           VkDeviceSize alignment) :
                  m_memoryTypeIndex(memoryTypeIndex),
                  m_memoryType(memoryType),
                  m_storeSize(size),
                  m_alignment(alignment),
                  m_deviceMemory(device, {VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO, nullptr,
                                          m_storeSize,
                                          m_memoryTypeIndex})
{}

VkDeviceSize DeviceMemoryBank::MemoryStore::size() const
{
    return m_storeSize;
}

VkDeviceSize DeviceMemoryBank::MemoryStore::alignment() const
{
    return m_alignment;
}

uint32_t DeviceMemoryBank::MemoryStore::memoryTypeIndex() const
{
    return m_memoryTypeIndex;
}

bool DeviceMemoryBank::MemoryStore::isHostVisible() const
{
    return m_memoryType.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
}

VkDevice DeviceMemoryBank::MemoryStore::vulkanDevice() const
{
    return m_deviceMemory.vulkanDevice();
}

VkDeviceMemory DeviceMemoryBank::MemoryStore::memoryHandle() const
{
    return m_deviceMemory.handle();
}

MemoryBinding::MemoryBinding(MemoryStore* memoryStore, VkDeviceSize size, VkDeviceSize offset) :
                m_memoryStore(memoryStore), m_size(size), m_offset(offset)
{}

MemoryBinding::~MemoryBinding()
{
    //if(valid())
    //  free();
}

MemoryBinding::MemoryBinding(MemoryBinding&& other) noexcept :
                m_memoryStore(nullptr), m_size(0), m_offset(0)
{
    *this = std::move(other);
}

MemoryBinding& MemoryBinding::operator=(MemoryBinding&& other) noexcept
{
    if(this != std::addressof(other))
    {
        // release resources held by this instantiation
        // if(valid())
			//free();

        // copy basic members
        m_offset = other.m_offset;
        m_size = other.m_size;

        // make sure the other one doesn't release the resources when it's destroyed.
        other.m_offset = 0;
        other.m_size  = 0;
    }
    return *this;
}

void MemoryBinding::load(Span<const std::byte> data)
{
    if(!valid())
        throw std::runtime_error("Trying to load data using invalid MemoryBinding. Was this binding moved on from?");
    if(data.size() > m_size)
        throw std::runtime_error("Trying to load data of the wrong size.");
    if(!m_memoryStore->isHostVisible())
        throw std::runtime_error("Trying to load data directly into memory invisible to host.");

    void* mappedMemory;
    VkResult mapResult = vkMapMemory(m_memoryStore->vulkanDevice(),
                                     m_memoryStore->memoryHandle(),
                                     m_offset, VK_WHOLE_SIZE,
                                     0, &mappedMemory);
    if(mapResult != VK_SUCCESS) throw std::runtime_error("Memory map failed.");
    // copy the contents
    std::memcpy(mappedMemory, data.data(), data.size());
    // unmap, avoiding further writes
    vkUnmapMemory(m_memoryStore->vulkanDevice(), m_memoryStore->memoryHandle());
}

bool MemoryBinding::isHostVisible() const
{
	if(m_memoryStore != nullptr)
		return m_memoryStore->isHostVisible();
    return false;
}

bool MemoryBinding::valid() const
{
    return (m_memoryStore != nullptr && m_size > 0);
}

DeviceMemoryBank::DeviceMemoryBank(VkPhysicalDevice physicalDevice, VkDevice device) :
	m_physicalDevice(physicalDevice),
	m_device(device)
{
    vkGetPhysicalDeviceMemoryProperties(m_physicalDevice, &m_memoryProperties);
}

DeviceMemoryBank::DeviceMemoryBank(DeviceMemoryBank&& other) noexcept :
    m_physicalDevice(other.m_physicalDevice),
    m_device(other.m_device),
    m_memoryProperties(other.m_memoryProperties)
	// Not bothering with the vector move right now, because we are only moving if the vector is empty anyway. Needs to be changed later.
{
    // This way we are sure we are not accidentally leaving dangling references.
	assert(other.m_memoryStores.size() == 0);
}

DeviceMemoryBank& DeviceMemoryBank::operator=(DeviceMemoryBank&& other) noexcept
{
    if(this != std::addressof(other))
    {
        // This way we are sure we are not accidentally leaving dangling references.
        assert(other.m_memoryStores.size() == 0);
		assert(m_memoryStores.size() == 0);

        m_physicalDevice = other.m_physicalDevice;
        m_device = other.m_device;
        m_memoryProperties = other.m_memoryProperties;
    }

    return *this;
}

DeviceMemoryBank::MemoryBinding DeviceMemoryBank::bind(VkBuffer buffer,
                                                       VkMemoryRequirements requirements,
                                                       bool hostVisible)
{
    size_t memoryStoreIndex = findMemoryStore(requirements, hostVisible);
    MemoryStore& memoryStore = m_memoryStores[memoryStoreIndex];
    VkResult bindResult = vkBindBufferMemory(m_device, buffer, memoryStore.memoryHandle(), 0);
    if(bindResult != VK_SUCCESS) throw std::runtime_error("Buffer binding failed.");
    return {std::addressof(m_memoryStores[memoryStoreIndex]), requirements.size, 0};
}


DeviceMemoryBank::MemoryBinding DeviceMemoryBank::bind(VkImage image, VkMemoryRequirements requirements, bool hostVisible)
{
    size_t memoryStoreIndex = findMemoryStore(requirements, hostVisible);
    MemoryStore& memoryStore = m_memoryStores[memoryStoreIndex];
    VkResult bindResult = vkBindImageMemory(m_device, image, memoryStore.memoryHandle(), 0);
    if(bindResult != VK_SUCCESS) throw std::runtime_error("Buffer binding failed.");
    return {std::addressof(m_memoryStores[memoryStoreIndex]), requirements.size, 0};
}

size_t DeviceMemoryBank::findMemoryStore(VkMemoryRequirements memoryRequirements,
                                           bool hostVisible)
{
    auto memoryInfo = getMemoryTypeIndex(memoryRequirements, hostVisible);
    m_memoryStores.emplace_back(m_device, memoryInfo.first, memoryInfo.second,
                                memoryRequirements.size,
                                memoryRequirements.alignment);
    return m_memoryStores.size() - 1;
}

std::pair<uint32_t, VkMemoryType> DeviceMemoryBank::getMemoryTypeIndex(VkMemoryRequirements memoryRequirements,
                                                                       bool hostVisible)
{
    for(uint32_t memoryType = 0; memoryType < 32; ++memoryType)
    {
        if(memoryRequirements.memoryTypeBits & (1u << memoryType))
        {
            if(!hostVisible || (m_memoryProperties.memoryTypes[memoryType].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT))
                return {memoryType, m_memoryProperties.memoryTypes[memoryType]};
        }
    }
    throw std::runtime_error("Unable to find an appropriate memory type.");
}