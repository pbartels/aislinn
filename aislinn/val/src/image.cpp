//
// Created by pieterjan on 17/12/2020.
//

#include "val/image.h"

using namespace aislinn::val;

Image::Image(VkDevice device, DeviceMemoryBank& memoryBank, VkImageCreateInfo imageCreateInfo, bool hostVisible) :
                                    m_image(device, imageCreateInfo),
                                    m_vulkanMemoryRequirements(device, m_image.handle()),
                                    m_memoryBinding(memoryBank.bind(m_image.handle(),
                                                                    m_vulkanMemoryRequirements.m_vulkanMemoryRequirements,
                                                                    hostVisible))
{}

Image::Image(VkDevice device, DeviceMemoryBank& memoryBank, VkImageCreateInfo imageCreateInfo, Span<const std::byte> data) :
                                    Image(device, memoryBank, imageCreateInfo, true)
{
    m_memoryBinding.load(data);
}

VkImage Image::handle() const
{
    return m_image.handle();
}
const VkImage* Image::pointer() const
{
    return m_image.pointer();
}

VkMemoryRequirements Image::memoryRequirements() const
{
    return m_vulkanMemoryRequirements.m_vulkanMemoryRequirements;
}

bool Image::hostVisible() const
{
    return m_memoryBinding.isHostVisible();
}

Image::VulkanMemoryRequirements::VulkanMemoryRequirements(VkDevice device, VkImage image)
{
    vkGetImageMemoryRequirements(device, image, &m_vulkanMemoryRequirements);
}