//
// Created by pieterjan on 06/11/2020.
//

#include <algorithm>
#include <exception>
#include <iostream>
#include <vector>

#include "val/vulkanbackend.h"

using namespace aislinn::val;


QueueHandle::QueueHandle(VkDevice device, uint32_t familyIndex, uint32_t queueIndex, VkQueue vulkanQueue) :
                            m_vulkanDevice(device),
                            m_familyIndex(familyIndex),
                            m_queueIndex(queueIndex),
                            m_vulkanQueue(vulkanQueue)
{}


VkDevice QueueHandle::device() const
{
    return m_vulkanDevice;
}

uint32_t QueueHandle::family() const
{
    return m_familyIndex;
}

uint32_t QueueHandle::queueIndex() const
{
    return m_queueIndex;
}

VkQueue QueueHandle::handle() const
{
    return m_vulkanQueue;
}


// Check if this object is in a state that is valid for use in rendering.
bool QueueHandle::isValid() const
{
    assert(m_vulkanDevice != VK_NULL_HANDLE || m_vulkanQueue == VK_NULL_HANDLE);
    return m_vulkanQueue != VK_NULL_HANDLE;
}

VulkanBackEnd::VulkanBackEnd(const Config& config) :
                            m_vulkanInstanceWrapper(),
                            m_vulkanDeviceWrapper()
{
    // Create the application info - not strictly necessary but better to do so.
    VkApplicationInfo appInfo{VK_STRUCTURE_TYPE_APPLICATION_INFO,nullptr,
                              config.m_applicationName.c_str(),
                              config.m_applicationVersion,
                              "Vulkan", 1,
                              VK_MAKE_VERSION(1, 0, 0)};

    // Query the available instance layers
    uint32_t numInstanceLayers = 0;
    std::vector<VkLayerProperties> instanceLayerProperties;
    vkEnumerateInstanceLayerProperties(&numInstanceLayers, nullptr);
    instanceLayerProperties.resize(numInstanceLayers);
    vkEnumerateInstanceLayerProperties(&numInstanceLayers, instanceLayerProperties.data());

    // check if the requested layers are available.
    for(const char* layerName: config.m_vulkanInstanceLayersToUse)
    {
        std::string layerNameString(layerName);
        bool found = false;
        for(VkLayerProperties& layerProperties: instanceLayerProperties)
        {
            if(layerProperties.layerName == layerNameString) found = true;
        }
        if(!found)
        {
            std::cout << "Did not find layer: " << layerName << ". Aborting." << std::endl;
            throw std::runtime_error("Requested layer not available.");
        }
    }

    // Query the available instance extensions.
    uint32_t numInstanceExtensions = 0;
    std::vector<VkExtensionProperties> instanceExtensionProperties;
    vkEnumerateInstanceExtensionProperties(nullptr, &numInstanceExtensions, nullptr);
    instanceExtensionProperties.resize(numInstanceExtensions);
    vkEnumerateInstanceExtensionProperties(nullptr, &numInstanceExtensions, instanceExtensionProperties.data());

    // check if the requested extensions are available.
    for(const char* extensionName: config.m_vulkanInstanceExtensionsToUse)
    {
        bool found = false;
        std::string extensionNameString(extensionName);
        for(VkExtensionProperties& extensionProperties: instanceExtensionProperties)
        {
            if(extensionProperties.extensionName == extensionNameString) found = true;
        }
        if(!found)
        {
            std::cout << "Did not find extension: " << extensionName << ". Aborting." << std::endl;
            throw std::runtime_error("Requested extension not available.");
        }
    }

    // The create info struct contains all parameters/options for creating the instance
    VkInstanceCreateInfo createInfo{VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO, nullptr,
                                    0, &appInfo,
                                    (uint32_t) config.m_vulkanInstanceLayersToUse.size(),
                                    config.m_vulkanInstanceLayersToUse.data(),
                                    (uint32_t) config.m_vulkanInstanceExtensionsToUse.size(),
                                    config.m_vulkanInstanceExtensionsToUse.data()};

    m_vulkanInstanceWrapper = InstanceWrapper(createInfo);

    selectVulkanPhysicalDevice(config.m_preferredVendorID);

    std::vector<int> familyQueueCount(m_queueFamilyProperties.size(), 0);
    // We first create a list of these structs, containing structs detailing what queues and how many to create
    std::vector<VkDeviceQueueCreateInfo> queuesToCreate;
    for(auto queueConfig: config.m_queueConfigs)
    {
        // find a queue family that is suitable for the config.
        for(size_t familyIndex = 0; familyIndex < m_queueFamilyProperties.size(); ++familyIndex)
        {
            VkQueueFamilyProperties queueProperties = m_queueFamilyProperties[familyIndex];
            if((queueProperties.queueFlags & queueConfig.m_queueRequirements) == queueConfig.m_queueRequirements)
            {
                // this queue is suitable for this
                queuesToCreate.push_back({VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, nullptr,
                                          0, (uint32_t) familyIndex, queueConfig.m_count,
                                          &queueConfig.m_priority});
                for(uint32_t queueIndex = 0; queueIndex < queueConfig.m_count; ++queueIndex)
                {
                    m_queueIndices.push_back({(uint32_t) familyIndex,
                                              (uint32_t) familyQueueCount[familyIndex] + queueIndex});
                }
                familyQueueCount[familyIndex] += queueConfig.m_count;
                continue;
            }
        }
    }

    // We then combine everything into a struct with creation details for the logical device.
    VkDeviceCreateInfo deviceCreateInfo{VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO, nullptr, 0,
                                        (uint32_t) queuesToCreate.size(), queuesToCreate.data(),
                                        (uint32_t) config.m_vulkanDeviceLayersToUse.size(), config.m_vulkanDeviceLayersToUse.data(),
                                        (uint32_t) config.m_vulkanDeviceExtensionsToUse.size(), config.m_vulkanDeviceExtensionsToUse.data(),
                                        &config.m_requestedDeviceFeatures};

    m_vulkanDeviceWrapper = DeviceWrapper(m_vulkanPhysicalDevice, deviceCreateInfo);
}

VkInstance VulkanBackEnd::vulkanInstance() const
{
    return m_vulkanInstanceWrapper.handle();
}

VkPhysicalDevice VulkanBackEnd::vulkanPhysicalDevice() const
{
    return m_vulkanPhysicalDevice;
}

VkPhysicalDeviceProperties VulkanBackEnd::physicalDeviceProperties() const
{
    return m_physicalDeviceProperties;
}

VkDevice VulkanBackEnd::vulkanDevice() const
{
    return m_vulkanDeviceWrapper.handle();
}

size_t VulkanBackEnd::queueCount() const
{
    return m_queueIndices.size();
}

QueueHandle VulkanBackEnd::queue(size_t index) const
{
    const QueueIndex& qIndex = m_queueIndices[index];
    VkQueue result;
    vkGetDeviceQueue(vulkanDevice(), qIndex.m_familyIndex, qIndex.m_queueIndex, &result);
    return QueueHandle(vulkanDevice(), qIndex.m_familyIndex, qIndex.m_queueIndex, result);
}

void VulkanBackEnd::logDetails() const
{
    // print some details of the selected device
    std::cout << "Aislinn rendering on: " << m_physicalDeviceProperties.deviceName << std::endl;
    std::cout << "\tVendor ID: " << m_physicalDeviceProperties.vendorID << std::endl;
    std::cout << "\tsupports vulkan version: " << VK_VERSION_MAJOR(m_physicalDeviceProperties.apiVersion) << "."
              << VK_VERSION_MINOR(m_physicalDeviceProperties.apiVersion) << "."
              << VK_VERSION_PATCH(m_physicalDeviceProperties.apiVersion) << std::endl;


    // print out the queues as found in the properties
    std::cout << "\tAvailable Queues:" << std::endl;
    for(uint32_t i = 0; i < m_queueFamilyProperties.size(); ++i)
    {
        const VkQueueFamilyProperties& properties = m_queueFamilyProperties[i];
        std::cout << "\t\t" << i << ": " << properties.queueCount << ": ";
        if(properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) std::cout << "drawing ";
        if(properties.queueFlags & VK_QUEUE_COMPUTE_BIT) std::cout << "compute ";
        if(properties.queueFlags & VK_QUEUE_TRANSFER_BIT) std::cout << "transfer ";
        std::cout << std::endl;
    }

    // Print out the created queues:
    std::cout << "\tCreated queues: " << std::endl;
    for(const QueueIndex& index : m_queueIndices)
    {
        std::cout << "\t\tQueue family: " << index.m_familyIndex << ", index: " << index.m_queueIndex << std::endl;
    }
}

void VulkanBackEnd::selectVulkanPhysicalDevice(uint32_t preferredVendorID)
{
    // first get the number of devices:
    uint32_t deviceCount;
    vkEnumeratePhysicalDevices(m_vulkanInstanceWrapper.handle(), &deviceCount, nullptr);
    // then to get the information in a suitable container:
    std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
    vkEnumeratePhysicalDevices(m_vulkanInstanceWrapper.handle(), &deviceCount, physicalDevices.data());

    // For now the logic is based on choosing the Nvidia device over the Intel device
    // Pick the first one in case there's no Nvidia device.
    m_vulkanPhysicalDevice = physicalDevices[0];
    for(VkPhysicalDevice device: physicalDevices)
    {
        // query the properties
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(device, &properties);
        if(properties.vendorID == preferredVendorID)
        {
            m_vulkanPhysicalDevice = device;
            continue;
        }
    }

    vkGetPhysicalDeviceProperties(m_vulkanPhysicalDevice, &m_physicalDeviceProperties);

    // Query the queue properties
    uint32_t propertyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(m_vulkanPhysicalDevice, &propertyCount, nullptr);
    m_queueFamilyProperties.resize(propertyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(m_vulkanPhysicalDevice, &propertyCount, m_queueFamilyProperties.data());
}

