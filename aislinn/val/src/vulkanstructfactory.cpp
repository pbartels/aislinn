//
// Created by pieterjan on 18/12/2020.
//

#include "val/vulkanstructfactory.h"

VkCommandPoolCreateInfo aislinn::val::vulkanstructs::commandPoolCreateInfo(uint32_t queueFamily, VkCommandPoolCreateFlags commandPoolCreateFlags)
{
    return {VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO, nullptr, commandPoolCreateFlags, queueFamily};
}

VkCommandBufferAllocateInfo aislinn::val::vulkanstructs::commandBufferAllocateInfo(VkCommandPool commandPool, bool primary, uint32_t count)
{
    return {VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, nullptr,
            commandPool, primary ? VK_COMMAND_BUFFER_LEVEL_PRIMARY : VK_COMMAND_BUFFER_LEVEL_SECONDARY, 1};
}

VkBufferCreateInfo aislinn::val::vulkanstructs::exclusiveBufferCreateInfo(VkBufferUsageFlags usageFlags, VkDeviceSize bufferSize)
{
    return {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO, nullptr, 0,
            bufferSize, usageFlags,
            VK_SHARING_MODE_EXCLUSIVE,
            // Because the sharing mode is exclusive, the following parameters are not used and can be 0, nullptr
            0, nullptr};
}

VkImageCreateInfo aislinn::val::vulkanstructs::stagingImageCreateInfo(uint32_t width, uint32_t height)
{
    return {VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, nullptr,
            0,
            VK_IMAGE_TYPE_2D,VK_FORMAT_R8G8B8A8_SRGB,
            {width, height, 1},
            1, 1,
            VK_SAMPLE_COUNT_1_BIT,
            VK_IMAGE_TILING_LINEAR,
            VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
            VK_SHARING_MODE_EXCLUSIVE,
            0, nullptr,
            VK_IMAGE_LAYOUT_PREINITIALIZED};
}

VkImageCreateInfo aislinn::val::vulkanstructs::textureImageCreateInfo(uint32_t width, uint32_t height)
{
    return {VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, nullptr, 0,
            VK_IMAGE_TYPE_2D, VK_FORMAT_R8G8B8A8_SRGB,
            {width, height, 1}, 1, 1,
            VK_SAMPLE_COUNT_1_BIT,
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_SHARING_MODE_EXCLUSIVE,
            0, nullptr,
            VK_IMAGE_LAYOUT_PREINITIALIZED};
}