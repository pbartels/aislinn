//
// Created by pieterjan on 06/11/2020.
//

#include "val/config.h"

using namespace aislinn::val;

QueueConfig::QueueConfig(VkQueueFlags queueRequirementFlags, uint32_t count, float priority) :
                                            m_queueRequirements(queueRequirementFlags),
                                            m_count(count),
                                            m_priority(priority)
{}

Config::Config(const std::string & applicationName,
               uint32_t applicationVersion,
               uint32_t preferredVendor,
               VkPhysicalDeviceFeatures requestedDeviceFeatures,
               Span<const char*> vulkanInstanceExtensionsToUse,
               Span<const char*> vulkanInstanceLayersToUse,
               Span<const char*> vulkanDeviceExtensionsToUse,
               Span<const char*> vulkanDeviceLayersToUse,
               Span<QueueConfig> queueConfigs) :
                               m_applicationName(applicationName),
                               m_applicationVersion(applicationVersion),
                               m_preferredVendorID(preferredVendor),
                               m_requestedDeviceFeatures(requestedDeviceFeatures),
                               m_vulkanInstanceExtensionsToUse(vulkanInstanceExtensionsToUse.begin(),
                                                               vulkanInstanceExtensionsToUse.end()),
                               m_vulkanInstanceLayersToUse(vulkanInstanceLayersToUse.begin(),
                                                           vulkanInstanceLayersToUse.end()),
                               m_vulkanDeviceExtensionsToUse(vulkanDeviceExtensionsToUse.begin(),
                                                             vulkanDeviceExtensionsToUse.end()),
                               m_vulkanDeviceLayersToUse(vulkanDeviceLayersToUse.begin(),
                                                         vulkanDeviceLayersToUse.end()),
                               m_queueConfigs(queueConfigs.begin(), queueConfigs.end())
{}
