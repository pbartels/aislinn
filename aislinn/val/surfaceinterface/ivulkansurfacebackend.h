//
// Created by pieterjan on 02/12/2020.
//

#ifndef AISLINN_VULKANSURFACEBACKEND_H
#define AISLINN_VULKANSURFACEBACKEND_H

#include <functional>

#include "vulkan/vulkan.h"

#include "aislinnutilities.h"

namespace aislinn
{
/**
 *  This is an interface class representing an IO and windowing backend.
 *  This lets us define the render loop in general terms, and the interface can be implemented using whatever backend
 *  that is available on the target system.
 *
 *  The goal for this class is to not use any Aislinn class. It should be as orthogonal as possible,
 *  with the intersection between the two purely focused on 'user IO': window creation and closure, buttons pressed, etc.
 *
 *  It is part of Aislinn to provide a common framework, to allow one to easily write cross-platform renderers using Aislinn.
 *  However, it does not have to be used, and all of these could be done a different way.
 *
 *  It's main use is helping create the aislinn::SurfaceInitializer functor.
 */
class IVulkanSurfaceBackEnd : private NonMovable
{
public:
    virtual void createSurface(VkInstance instance, VkSurfaceKHR* surface) = 0;

    // turn the createSurface member function into a functor (without the ugly bind stuff).
    inline std::function<void(VkInstance, VkSurfaceKHR*)> surfaceInitializer()
    {
        return [this](VkInstance instance, VkSurfaceKHR* surfaceHandle){this->createSurface(instance, surfaceHandle);};
    }
};
}

#endif //AISLINN_VULKANSURFACEBACKEND_H
